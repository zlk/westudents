// Karma configuration
// Generated on Sun Jul 06 2014 18:24:31 GMT-0400 (Eastern Daylight Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [{pattern:'app/bower_components/angular/angular.js'},
            {pattern:'app/bower_components/angular-mocks/angular-mocks.js'},
            'app/bower_components/angular-resource/angular-resource.js',
            'app/bower_components/angular-route/angular-route.js',
            'app/bower_components/angular-translate/angular-translate.min.js',
            {pattern:'app/bower_components/jquery/dist/jquery.js'},
            {pattern:'app/bower_components/bootstrap/dist/js/bootstrap.js'},
            {pattern:'app/bower_components/angular-bootstrap/ui-bootstrap.js'},
            {pattern:'app/bower_components/angular-bootstrap/ui-bootstrap-tpls.js'},
            {pattern:'node_modules/requirejs/require.js'},
            'app/scripts/config.js',
            //'app/scripts/components/menu-list/menu-list-directive.js',

            //'app/scripts/services/address-service.js',
            //'app/scripts/services/search-service.js',
            //'app/scripts/services/school-service.js',
            
            //'app/scripts/components/wizard/wizard-directive.js',
            //'app/scripts/components/datepicker/datepicker-directive.js',
            //'app/scripts/components/dual-list/dual-list-directive.js',
            
            //'app/scripts/address/address-controller.js',
            
        	//'app/scripts/legal/terms-controller.js',
        	//'app/script/legal/privacy-controller.js',
            //'app/scripts/header/header-directive.js',
            //'app/scripts/signup/signup-form-directive.js',
            
            //'app/scripts/item-picture/item-picture-service.js',
            //'app/scripts/item-picture/item-picture-directive.js',
            
            //'app/scripts/item-list/item-list-service.js',
            //'app/scripts/item-list/item-list-directive.js',
            
            //'app/scripts/multi-uploader/multi-uploader-directive.js',
            //'app/scripts/item-detail/item-detail-service.js',
            //'app/scripts/item-detail/item-detail-directive.js',
            //'app/scripts/dashboard/dashboard-controller.js',
            
            
            //'app/scripts/home/home-controller.js',
            //'app/scripts/controllers/main.js',
            //{pattern:'app/scripts/**/*.js'},
            //'app/scripts/item-picture/item-picture-directive.js',
            
            //'app/scripts/components/image-uploader/image-uploader-service.js',
            
            
            //'app/scripts/login/login-service.js',
            'app/scripts/services/main-service.js',
            //'app/scripts/login/login-controller.js',
            //'app/scripts/signup/signup-controller.js',
            //'app/scripts/app.js',
      //'test-main.js',
      //'test/unit/address/city-controller.js',//broken
	  //'test/unit/address/province-controller.js',
	  //'test/spec/directives/item-list.js',
      //'test/spec/controllers/main.js',
      
            //'test/spec/login/login-controller.js',
            //'test/spec/signup/signup-controller.js',
        /*
        
        
        'test/spec/home/home-controller.js',
        'test/spec/services/address-service.js',
        'test/spec/search/search-service.js',
        'test/spec/school/school-service.js'
        */
         //'test/spec/components/image-uploader-service.js'
		 'test/spec/services/main-service.js',
    ],


    // list of files to exclude
    exclude: [
        'app/scripts/app.js'
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],

    
    
    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  });
};
