//--------------------------------------------------------------------------------------------------
//	Author: Like Zhang 2015-03-07
//	2014-2015 Copyright reserved
//--------------------------------------------------------------------------------------------------

'use strict';
var MAX_N_PICTURES = 8;
var RESTFUL_URL = 'api';

var ItemError = {
		NONE:0,
		EMPTY:1,
		NOT_NUMBER:2,
		INVALID_CHAR:3
};


var GeoCodeStatus = {
	OK:0,
	ERROR:1
}

//angular.module('underscore', []).factory('_', function() {
//	return window._; // assumes underscore has already been loaded on the page
//});

angular.module('mainModule').controller('ApplicationFormController',
		[ '$scope', '$rootScope', '$location', '$http', 'MainService', function($scope, $rootScope, $location, $http, $ms){
			
		    //------------------------------------------------------------------------------------
			//------------------------------------------------------------------------------------
			function _init(){
				$scope.item = { name: '', gender:'', age:'', email: '', school:'', major:'', gpa:'', message: ''};
				$scope.error = { name: '', gender:'', age:'', email: '', school:'', major:'', gpa:'', message: ''};
				$scope.planYears = [2016, 2017, 2018];
				$scope.currEnrolls = [{id:0, title:'University Grade 4'},
				                         {id:1, title:'University Grade 3'},
				                         {id:2, title:'College Grade 3'},
				                         {id:3, title:'College Grade 2'},
				                         {id:4, title:'Postgraduated and Work'},
				                         {id:5, title:'Postgraduated Studing'},
				                         {id:6, title:'University graduated and Work'},
				                         {id:7, title:'College graduated more than 3 years'},
				                         {id:8, title:'College graduated less than 3 years'}];//Currently enrolled in grades
			
				$scope.majors = [{id:0, title:'Computer Science'},
									{id:1, title:'Chemistry'},
									{id:2, title: 'Animal'},
									{id:2, title: 'Plant'},
									{id:3, title: 'Others'}];
				
				$scope.schools = [{id: 0, selected: false, name:'Conestoga College', type: 'College', imageID:'conestoga'},
				                  {id: 1, selected: false, name:'Fanshawe College', type: 'College', imageID:'fanshawe'},
				                  {id: 2, selected: false, name:'Guelph University', type: 'University',imageID:'guelph'},
				                  {id: 3, selected: false, name:'Toronto University', type: 'University',imageID:'toronto'},
				                  {id: 4, selected: false, name:'Waterloo University', type: 'University',imageID:'waterloo'},
				                  {id: 5, selected: false, name:'Western Ontario University', type: 'University',imageID:'western'}
				                  ];
				
				$scope.consultants = [ {id:0, selected: false, name: 'Vicky', gender:'F', photoID: '001', school: 'Toronto University', schoolType: 'University', program: 'Business Accounting', degree: 'Master', current: 'Studing', nSuccessCases: 5 },
				                       {id:1, selected: false, name: 'Henry', gender:'M', photoID: '010', school: 'Toronto University', schoolType: 'University', program: 'Business Accounting', degree: 'Master', current: 'Studing', nSuccessCases: 2 },
				                       {id:2, selected: false, name: 'Kevin', gender:'M', photoID: '011', school: 'Conestoga College', schoolType: 'College', program: 'Computer Science', degree: 'Bachelor', current: 'Studing', nSuccessCases: 3 },
				                       {id:3, selected: false, name: 'Jenias', gender:'F', photoID: '002', school: 'Guelph University', schoolType: 'University', program: 'Plant Science', degree: 'Master', current: 'Studing', nSuccessCases: 2 },
				                       {id:4, selected: false, name: 'Vivian', gender:'F', photoID: '003', school: 'Guelph University', schoolType: 'University', program: 'Animal Science', degree: 'Master', current: 'Studing', nSuccessCases: 2 }
				                      ];
				
				$scope.steps = [{id:0, title:'Find Advisor', img:'compass-done'},
				                {id:1, title:'Fill Application', img:'write'},
				                {id:2, title:'Find School', img:'school'},
				                {id:3, title:'Pay Fee',img:'cart'},
				                {id:4, title:'Review Paper', img:'key'},
				                {id:5, title:'Contact Professor', img:'hat'}];
				
				$scope.activeIndex = 0;
				
				$scope.selectedConsultants = [];
				$scope.selectedSchools = [];
				
			}
			$scope.getImage = function(imageID){
				return '../images/schools/' + imageID + '.png';
			}
			$scope.getPhoto = function(photoID){
				return '../images/consultants/' + photoID + '.jpg';
			}
			$scope.setActivePage = function(k){
				$.each($scope.steps, function(i, step){
					var s = step.img.split('-');
					if(i==k){
						step.img = s[0] + '-done';
						$scope.activeIndex = k;
					}else{
						step.img = s[0];
					}
				})
			}
			
			$scope.toggleConsultant = function(id){
				$.each($scope.consultants, function(i, c){
					if(c.id == id){
						c.selected = ! c.selected;
						if(c.selected){
							$scope.selectedConsultants.push(c);
						}else{
							$scope.selectedConsultants = $.grep($scope.selectedConsultants, function(v) {
								  return v.id != c.id;
							});
						}
						return;
					}
				});
			}
			
			$scope.toggleSchool = function(id){
				$.each($scope.schools, function(i, c){
					if(c.id == id){
						c.selected = ! c.selected;
						if(c.selected){
							$scope.selectedSchools.push(c);
						}else{
							$scope.selectedSchools = $.grep($scope.selectedSchools, function(v) {
								  return v.id != c.id;
							});
						}
						return;
					}
				});
			}
			
			$scope.submit = function(){
	            $ms.showConfirmDialog({
	                title: 'Confirm',
	                body: "Do you want to submit your application?",
	                onAfterConfirm: function (e) {
//	                    _clearErrors();
//	                    _targetPage = null;
//	                    $cs.resetCustomerParam();
//	                    _loadFormData(_account);
//	                    $cs.setFormMode(FormMode.NEW);
//	                    _selectWizardPager(0);
	                }
	            });
			}
			
			//----------------------------------------------------------------------------------
			// Callback of the city select directive
			// Argument:
			//		val 		--- input of the auto complete control
			//		callback 	--- func([])
			//----------------------------------------------------------------------------------
			$scope.getCitiesCallback = function(val, callback){
				AddrService.searchCities({city:{'$include':val}}, function(cities){
					if(callback){
						callback(cities);
					}
				});
			}
			
			
			//----------------------------------------------------------------------------------
			// Private methods
			//----------------------------------------------------------------------------------
			
			
			function validate(values){
				var rules = {
						'description':{ 
							'required' : {},
							'validChars' : {},
							'maxlength': {'params': 4096}
							}, //end of description
						'price': { 
							'required' : {},
							'numeric' : {}
							}, //end of price	
						'province':{
							'required':{'message':'Please select a province.'},
							'validChars' : {}
							}, //end of province
						'city':{
							'cityNotEmpty':{
								'func':function(){
									return $scope.selectedText != '';
								},
								'message':'Please select a city.'
							},
							'validChars' : {'message':'The city does not exist.'}
							}, //end of city
						'street':{
							'required':{},
							'validChars' : {}
							}, //end of street
						'unit':{
							'validChars' : {}
							}, //end of unit
						'postcode':{
							'validChars' : {}
							}, //end of postcode
						'nbedrooms': { 
							'required' : {'message': 'Number of bedrooms cannot be empty.'},
							'numeric' : {}
							}, //end of nbedrooms
						/*'nbaths': { 
							'required' : {'message': 'Number of baths cannot be empty.'},
							'numeric' : {}
							}, //end of nbaths
						'nparkings': { 
							'required' : {'message': 'Number of parkings cannot be empty.'},
							'numeric' : {}
							}, //end of nparkings
							*/
						'email':{
								'validChars' : {},
								'mustLeaveContact': {
									'func': function(){
										return $scope.item.email!='' || $scope.item.phone!='';
									},
									'message': 'You must provide email or phone.'
								}
							}, //end of email
						'phone':{
								'validChars' : {}
							}, //end of phone
						}
				
				//return ValidateService.validate(values, rules);
			}
			
			// Display error message from backend
			function errorsHandler(errors){
				var fields = Object.keys(errors);				
			}
			
			
			/*
			$scope.$watch("errMsgs.city", function(newVal, oldVal){
				if(newVal){
					$scope.errMsgs.city = "";
				}
			});
			
			$scope.$watch("errMsgs.street", function(newVal, oldVal){
				if(newVal){
					$scope.errMsgs.street = "";
				}
			});
			
			$scope.$watch("errMsgs.description", function(newVal, oldVal){
				if(newVal){
					$scope.errMsgs.description = "";
				}
			});
			
			$scope.$watch("errMsgs.price", function(newVal, oldVal){
				if(newVal){
					$scope.errMsgs.price = "";
				}
			});
			
			$scope.$watch("errMsgs.nbedrooms", function(newVal, oldVal){
				if(newVal){
					$scope.errMsgs.nbedrooms = "";
				}
			});
			
			$scope.$watch("errMsgs.email", function(newVal, oldVal){
				if(newVal){
					$scope.errMsgs.email = "";
				}
			});
			
			$scope.$watch("errMsgs.phone", function(newVal, oldVal){
				if(newVal){
					$scope.errMsgs.phone = "";
				}
			});
			*/

			_init();
			
		} ]);
