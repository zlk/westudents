//--------------------------------------------------------------------------------------------------
//	Author: Like Zhang 2015-03-07
//	2014-2015 Copyright reserved
//--------------------------------------------------------------------------------------------------

var MAX_IMAGE_WIDTH = 640;
var MAX_IMAGE_HEIGHT = 480;

var N_MAX_IMAGES = 8;
var ERROR_CODE = {
	NONE:0,
	OUT_OF_RANGE:-1
};

angular.module('mainModule')

.factory('UploadService', function(){

	function dataURItoBlob(dataURI) {
	    // convert base64/URLEncoded data component to raw binary data held in a string
	    var byteString;
	    if (dataURI.split(',')[0].indexOf('base64') >= 0){
	        byteString = atob(dataURI.split(',')[1]);
	    }else{
	        byteString = unescape(dataURI.split(',')[1]);
	    }
	    // separate out the mime component
	    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

	    // write the bytes of the string to a typed array
	    var ia = new Uint8Array(byteString.length);
	    for (var i = 0; i < byteString.length; i++) {
	        ia[i] = byteString.charCodeAt(i);
	    }

	    return new Blob([ia], {type:mimeString});
	}
	
	function fillForm(formData, itemId, data){
		var info = {_id:itemID};
		// ----------------------------------------------------------------------
		// void append(DOMString name, DOMString value)
		// ----------------------------------------------------------------------
		formData.append('item_id', itemId);
		

		var info = {"username": d.username, "fname":d.fname });
		
		formData.append('info', JSON.stringify(info));
		
		var dataURL = data;
		// only http.post images with data, ignore non updated images, the blob will be handled in busboy.on('file') in backend
		if(dataURL!=null && dataURL!=""){	
			// blob must be the last fields
			var blob = dataURItoBlob(dataURL);
			formData.append('f_ps', blob);
		}
	}
	
	return{
		
		dataURItoBlob: function(dataURI) {
		    return dataURItoBlob(dataURI);
		},
		
		fillForm: function(formData, itemId, data){
			fillForm(formData, itemId, data);
		},
		
		// ----------------------------------------------------------------------
		// Note: only submit the input which blob is not empty
		// formData {id:item_id, f0:blob0, f1:blob1, f2:blob2 ...}
		//	Arguments:
		//		itemId -- [string]
		//		oReq = new XMLHttpRequest();
		// ----------------------------------------------------------------------
		submit: function(oReq, itemId, data){
			var formData = new FormData();
			fillForm(formData, itemId, data);
			
			// ----------------------------------------------------------------------
			// void open(DOMString method, DOMString url, optional boolean async, 
			//		optional DOMString? user, optional DOMString? password)
			// ----------------------------------------------------------------------
			oReq.open("post", "/api/uploadDoc", true);
			
			// ----------------------------------------------------------------------
			// You must call setRequestHeader() after open(), but before send(). 
			// If this method is called several times with the same header, the 
			// values are merged into one single request header.
			// ----------------------------------------------------------------------
			//oReq.setRequestHeader('Content-Type', "multipart/form-data;boundry:xxxx");
			
			// ----------------------------------------------------------------------
			// void send(FormData data);
			// ----------------------------------------------------------------------
		    oReq.send(formData);
		}
		
		

	}
})