//--------------------------------------------------------------------------------------------------
//	Author: Like Zhang 2015-03-07
//	2014-2015 Copyright reserved
//--------------------------------------------------------------------------------------------------
angular.module('mainModule').factory('MapService', ['$http', function($http){
	
	var infoWindow;
	
	//--------------------------------------------------------------------------------------
	// Draw map with single marker
	// options --- { id: DOMId, zoom:int, type: mapType, 
	//	marker: { title: string, lat:long, lng:long }}
	//--------------------------------------------------------------------------------------
	function drawMap(options){
		markerOptions = options.marker;
		var myOptions = {
				center : new google.maps.LatLng(markerOptions.lat, markerOptions.lng),
				zoom : options.zoom,
				mapTypeId : options.type,
				scrollwheel: false,
		};
	
		var map = new google.maps.Map(document.getElementById(options.id), myOptions);

		var marker = new google.maps.Marker({
			position : new google.maps.LatLng(markerOptions.lat, markerOptions.lng),
			zoom : options.zoom,
			title: markerOptions.title,
			map : map
		});
	
		google.maps.event.trigger(map, 'resize');
		
		google.maps.event.addListener(marker, 'mouseover', function() {
			infoWindow = new google.maps.InfoWindow({
				//content: '<div class="info-window">'+ 'Hands of Care, Unit 8C, 450 Westheights Dr Kichener ON N2N 1M2 ' + '<a title="Click to view our website" href="#"Our website</a></div>'
			});
			infowindow.open(map, marker);
		});
		
		google.maps.event.addListener(marker, 'mouseout', function() {
			if(typeof infowindow !="undefined")
			  infowindow.close();
		});
	}
	
	
	return{
		//--------------------------------------------------------------------------------------
		// Draw map with single marker
		// mapOptions --- { id: DOMId, type: mapType, 
		//		marker: { title: string, address: string }}
		//--------------------------------------------------------------------------------------
		drawMap : function(options){
			defaults = {
					id: '',
					zoom: 16,
					type:google.maps.MapTypeId.ROADMAP,
					marker:{
						title: '',
						address: '',
					}
			}
			
			options = $.extend({}, defaults, options);
			
			var c = {};
			var geocoder = new google.maps.Geocoder();
			geocoder.geocode({ 'address': options.marker.address }, function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						
						c['lat'] = results[0].geometry.location.lat();
						c['lng'] = results[0].geometry.location.lng();
					}else{
						c['lat'] = 0;
						c['lng'] = 0;							
					}
					drawMap({id:options.id, zoom:options.zoom, type:options.type, 
						marker:{ title: options.marker.title, 
								lat:c['lat'], 
								lng:c['lng']}
					});
					
				});	
		}// end of drawMap
	}
}]);