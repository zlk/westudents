//--------------------------------------------------------------------------------------------------
//	Author: Like Zhang 2015-03-07
//	2014-2015 Copyright reserved
//--------------------------------------------------------------------------------------------------

angular.module('mainModule',[])

.factory('MainService', ['$translate','$http', function($translate, $http){
	
	function getOrientation(){
		return window.screen.width > window.screen.height? 'landscape' : 'portrait';
	}
	
	function inMobile(){
		return window.screen.width < 768 || window.screen.height < 768;
	}
	//--------------------------------------------------------------------------------------
	// setSessionItem
	//	name --- name of session item
	// 	obj --- object that can be convert to json string  
	//--------------------------------------------------------------------------------------
	function _setSessionItem( name, obj ){
		if( typeof obj == 'undefined'){
			sessionStorage.setItem( cfg.sessionPrefix + name, null );
		}else if(typeof obj == 'string'){
			sessionStorage.setItem( cfg.sessionPrefix + name, obj);
		}else{
			if(obj)
				sessionStorage.setItem( cfg.sessionPrefix + name, JSON.stringify(obj));
			else
				sessionStorage.setItem( cfg.sessionPrefix + name, obj);
		}
	}
	
	//--------------------------------------------------------------------------------------
	// setLocalItem
	//	name --- name of session item
	// 	obj --- object that can be convert to json string  
	//--------------------------------------------------------------------------------------
	function _setLocalItem( name, obj ){
		if( typeof obj == 'undefined'){
			localStorage.setItem( cfg.sessionPrefix + name, null );
		}else if(typeof obj == 'string'){
			localStorage.setItem( cfg.sessionPrefix + name, obj);
		}else{
			if(obj)
				localStorage.setItem( cfg.sessionPrefix + name, JSON.stringify(obj));
			else
				localStorage.setItem( cfg.sessionPrefix + name, obj);
		}
	}
	
	
	function getPhoto(c){
		if(c && c.photo){
			return c.photo;
		}else{
			return '../images/consultants/001.jpg';
		}
	}
	
	return{
		changeLanguage: function(lang){
			if(lang=='en'){
				$translate.use('en_US');
			}else if(lang=='zh_CN'){
				$translate.use('zh_CN');
			}else{
				$translate.use('zh_CN');
			}
		},
		
		inMobile: function(){
			return inMobile();
		},
		
		renewToken: function(token, callback){
			$http.post(cfg.apiUrl + '/renewToken', {'token':token})
				.success(function(data, status, headers, config) {
					// data --- {'success': false, 'message': 'Invalid token', decoded: '', token: ''}
					if(data.success){
						if(callback)
							callback(data.token, '');
					}else{
						if(callback)
							callback('', data.message);
					}
				})
				.error(function(data, status, headers, config) {
					console.log('Failed to get token');
					if(callback)
						callback('', 'Invalid token');
				});
		},


		
		getObjectId:function(successCb, failCb){
			$http.get(RESTFUL_URL + '/object_id').success(
					function(data, status, headers, config) {
						if(successCb){
							successCb(data._id);
						}
					}).error(
					function(data, status, headers, config) {
						console.log('[failed] -- Get object id');
						if(failCb){
							failCb(null);
						}
					});
		},
		
		getSchools: function(successCb, errorCb){
			$http.post(cfg.apiUrl + '/getSchools', {})
				.success(function(data, status, headers, config) {
					if(successCb){
						successCb(data, status, headers, config);
					}
				})
				.error(function(){
					if(errorCb)
						errorCb();
				});
		},
		
		getPrograms: function(successCb, errorCb){
			$http.post(cfg.apiUrl + '/getPrograms', {})
				.success(function(data, status, headers, config) {
					if(successCb){
						successCb(data, status, headers, config);
					}
				})
				.error(function(){
					if(errorCb)
						errorCb();
				});
		},
		
		getSchoolList: function(successCb){
			var schools = [];
			this.getSchools(function(d){
				$.each(d.schools, function(i,v){
					if(["University of Toronto", "University of Guelph", "Wilfrid Laurier University","University of Waterloo","University of Western Ontario"].indexOf(v.name)!=-1){
						schools.push(v);
					}
				});
				
				if(successCb)
					successCb(schools);
			});
		},

		getConsultants: function(query, successCb, errorCb){
			if(query == null){
				query = {role: 2};
			}else{
				query = $.extend(query, {role:2});
			}
			
			$http.post(cfg.apiUrl + '/getUsers', query)
				.success(function(data, status, headers, config) {
					if(successCb){
						$.each(data.users, function(i,v){
							v.photo = getPhoto(v);
						})
						successCb(data, status, headers, config);
					}
				})
				.error(function(){
					if(errorCb)
						errorCb();
				});
		},
		
		resize: function(){
			var orientation = getOrientation();
			if(inMobile()){
				if(orientation == 'portrait'){
					if(window.innerWidth < 768){
						$('.page-content').height(window.innerHeight - $('.footer').height() - $('.nav-menus').height()- 40);						
					}else{
						$('.page-content').height(window.innerHeight - $('.footer').height() - $('.nav-menus').height()- 70);										
					}
				}	
			}else{
				$('.page-content').height(window.innerHeight - $('.footer').height() - $('.nav-menus').height() - 70);						
			}
		},
		
		//--------------------------------------------------------------------------------------
		// getSessionItem
		//	name --- name of session item 
		//--------------------------------------------------------------------------------------
		getSessionItem : function( name ){	
			var s = sessionStorage.getItem( cfg.sessionPrefix + name);
			if (s=='null')
				return null;
			try {
				if(s){
					return JSON.parse(s);
				}else{
					return s;
				}
		    } catch (e) {
		        return s;
		    }
		},
		
		//--------------------------------------------------------------------------------------
		// setSessionItem
		//	name --- name of session item
		// 	obj --- object that can be convert to json string  
		//--------------------------------------------------------------------------------------
		setSessionItem : function( name, obj ){
			if( typeof obj == 'undefined'){
				sessionStorage.setItem( cfg.sessionPrefix + name, null );
			}else if(typeof obj == 'string'){
				sessionStorage.setItem( cfg.sessionPrefix + name, obj);
			}else{
				if(obj)
					sessionStorage.setItem( cfg.sessionPrefix + name, JSON.stringify(obj));
				else
					sessionStorage.setItem( cfg.sessionPrefix + name, obj);
			}
		},
	
		//--------------------------------------------------------------------------------------
		// getLocalItem
		//	name --- name of session item 
		//--------------------------------------------------------------------------------------
		getLocalItem : function( name ){	
			var s = localStorage.getItem( cfg.sessionPrefix + name);
			if (s=='null')
				return null;
			try {
				if(s){
					return JSON.parse(s);
				}else{
					return s;
				}
		    } catch (e) {
		        return s;
		    }
		},
		
		//--------------------------------------------------------------------------------------
		// setLocalItem
		//	name --- name of session item
		// 	obj --- object that can be convert to json string  
		//--------------------------------------------------------------------------------------
		setLocalItem : function( name, obj ){
			if( typeof obj == 'undefined'){
				localStorage.setItem( cfg.sessionPrefix + name, null );
			}else if(typeof obj == 'string'){
				localStorage.setItem( cfg.sessionPrefix + name, obj);
			}else{
				if(obj)
					localStorage.setItem( cfg.sessionPrefix + name, JSON.stringify(obj));
				else
					localStorage.setItem( cfg.sessionPrefix + name, obj);
			}
		},
		
		//------------------------------------------------------------------------------------------
		// 	Convert the query object to string.
		//	Arguments: 
		//		query --- query object of mongodb format. eg. { item_id: itemId }
		//	Return:
		//		string of the query, eg. firstname=a&lastname=b
		//------------------------------------------------------------------------------------------
		toQueryStr: function(query){
			var list = [];
			if( !_.isEmpty(query) ){
				_.each(query, function(val, key){
					list.push(key + '=' + val);
				});
				return list.join('&');
			}
			return '';
		},

		
		securePost: function(url, options, successCb, errorCb){
			var token = this.getSessionItem('token');
			if(token){
				$http.post(cfg.apiUrl + url, $.extend({'token':token}, options) )
				.success(function(data, status, headers, config) {
					if(data.token){
						_setSessionItem('token', data.token);
					}
					
					if(!data.success){
						// if(window.location.hash=='#/propertyList'){
							// $location.path('/signin');
						// }
					}

					if(successCb){
						successCb(data, status, headers, config);
					}
				})
				.error(errorCb);
			}else{
				if(errorCb){
					errorCb(null, null, null, null);
				}
			}
		},
		
		//-------------------------------------------------
        //
        showAlertDialog: function(opt) {
            var $modal = $('#modal-dialog');

            if (opt && opt.title != null) {
                $modal.find('.modal-title').text(opt.title);
            }

            $modal.find('.modal-body p').text(opt.body);

            $modal.modal({
                backdrop: true,
                keyboard: true
            });

            $modal.find('.btn-yes').hide();
            $modal.find('.btn-no').hide();
            $modal.find('.btn-ok').show();
        },

        onAfterConfirm : null,
        onAfterCancel : null,
        
        showConfirmDialog : function (opt) {
        	var data = this;
            data.onAfterConfirm = opt.onAfterConfirm;
            data.onAfterCancel = opt.onAfterCancel;
            var $modal = $('#modal-dialog');

            if (opt.title != null) {
                $modal.find('.modal-title').text(opt.title);
            }

            $modal.find('.modal-footer button.btn-primary').click(function (e) {
                $('.w-popup').modal('hide');
                if (data.onAfterConfirm) {
                    data.onAfterConfirm(e);
                    data.onAfterConfirm = null;
                }
            });

            $modal.find('.modal-footer button.btn-no').click(function (e) {
                $('.w-popup').modal('hide');
                if (data.onAfterCancel) {
                    data.onAfterCancel(e);
                    data.onAfterCancel = null;
                }
            });

            $modal.find('.modal-body p').text(opt.body);
            $('.w-popup').modal({
                backdrop: true,
                keyboard: true
            });

            $modal.find('.btn-yes').show();
            $modal.find('.btn-no').show();
            $modal.find('.btn-ok').hide();
        },

        showSplashDialog : function (opt) {
            var $modal = $('#modal-dialog');
            if (opt.title != null) {
                $modal.find('.modal-title').text(opt.title);
            }

            $modal.find('.modal-body p').text(opt.body);
            $modal.modal({
                backdrop: true,
                keyboard: true
            });

            $modal.find('.btn-yes').hide();
            $modal.find('.btn-no').hide();
            $modal.find('.btn-ok').hide();

            var timeout = 1300;
            if (opt.timeout) {
                timeout = opt.timeout;
            }

            setTimeout(function () {
                $modal.modal('hide');

                if (opt.onAfter) {
                    opt.onAfter();
                }

            }, timeout);
        },
		
		getDateTimeString : function(dt){
			if(dt){
				var s = dt.split('GMT-');
				return s[0].trim();
			}else{
				return '5 minutes ago';
			}
		},
        

        indexOf : function (a, v) {
            for (var i = 0; i < a.length; i++) {
                if (JSON.stringify(a[i]) === JSON.stringify(v)) {
                    return i;
                }
            }
            return 0;
        },

        omit : function (a, key) {
            var s = {};
            $.each(a, function (k, v) {
                if (k != key) {
                    s[k] = v;
                }
            });
            return s;
        },

        isEmpty : function (obj) {
            if (obj == null) return true;
            if (Object.prototype.toString.call(obj) === '[object Array]' || typeof obj == "string")
                return obj.length === 0;
            if (typeof obj == 'object')
                return Object.keys(obj).length === 0;
        },

        keys : function (a) {
            var s = [];
            $.each(a, function (k, v) {
                s.push(k);
            });
            return s;
        },

        values : function (a) {
            var s = [];
            $.each(a, function (k, v) {
                s.push(v);
            });
            return s;
        },

        findWhere : function (a, obj) {
            if (a) {
                for (var i = 0; i < a.length; i++) {
                    var v = a[i];
                    var matched = true;

                    for (var key in obj) {
                        if (!(key in v && v[key] == obj[key])) {
                            matched = false;
                            break;
                        }
                    }

                    if (matched) {
                        return v;
                    }
                }
            }

            return null;
        }

        
		
	}
}])
.controller('mainController', [ 'MainService', '$scope', '$location', function(MainService, $scope, $location){
	$scope.vipService = function(){
		$location.path('vip-service');
	}
}])

