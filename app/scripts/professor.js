'use strict';

angular.module('mainModule')

.controller('ProfessorController', [ '$scope', '$location', 'MainService', function($scope, $location, $ms){
	$scope.currEnrolls = [{id:0, title:'University Grade 4'},
	                         {id:1, title:'University Grade 3'},
	                         {id:2, title:'College Grade 3'},
	                         {id:3, title:'College Grade 2'},
	                         {id:4, title:'Postgraduated and Work'},
	                         {id:5, title:'Postgraduated Studing'},
	                         {id:6, title:'University graduated and Work'},
	                         {id:7, title:'College graduated more than 3 years'},
	                         {id:8, title:'College graduated less than 3 years'}];//Currently enrolled in grades

	$scope.majors = [{id:0, title:'Computer Science'},
						{id:1, title:'Chemistry'},
						{id:2, title: 'Animal'},
						{id:2, title: 'Plant'},
						{id:3, title: 'Others'}];
	
	$scope.programs = [{id: 0, name:'Animal Science', school: 'Guelph University'},
	                    {id: 1, name:'Plant Science', school: 'Guelph University'},
	                    {id: 2, name:'Computer Science', school: 'Conestoga College'},
	                    {id: 3, name: 'Business Accounting', school:'Toronto University'}];
	
	$scope.schools = [{id: 0, selected: false, name:'Conestoga College', type: 'College', imageID:'conestoga'},
	                  {id: 1, selected: false, name:'Fanshawe College', type: 'College', imageID:'fanshawe'},
	                  {id: 2, selected: false, name:'Guelph University', type: 'University',imageID:'guelph'},
	                  {id: 3, selected: false, name:'Toronto University', type: 'University',imageID:'toronto'},
	                  {id: 4, selected: false, name:'Waterloo University', type: 'University',imageID:'waterloo'},
	                  {id: 5, selected: false, name:'Western Ontario University', type: 'University',imageID:'western'}];
	
	$scope.types = [{id:0, title:'Research Master'},
		               {id:1, title:'Taught Master'}];
	
	$scope.steps = [{title:'Find Advisor', img:'compass-done'},
	                {title:'Fill Application', img:'write'},
	                {title:'Find School', img:'school'},
	                {title:'Pay Fee',img:'cart'},
	                {title:'Review Paper', img:'key'},
	                {title:'Contact Professor', img:'hat'}];
	
	$scope.getPhoto = function(photoID){
		return '../images/consultants/' + photoID + '.jpg';
	}
	
	$scope.consultants = [ {name: 'Vicky', gender:'F', photoID: '001', school: 'Toronto University', schoolType: 'University', program: 'Business Accounting', degree: 'Master', current: 'Studing', nSuccessCases: 5 },
	                       {name: 'Henry', gender:'M', photoID: '010', school: 'Toronto University', schoolType: 'University', program: 'Business Accounting', degree: 'Master', current: 'Studing', nSuccessCases: 2 },
	                       {name: 'Kevin', gender:'M', photoID: '011', school: 'Conestoga College', schoolType: 'College', program: 'Computer Science', degree: 'Bachelor', current: 'Studing', nSuccessCases: 3 },
	                       {name: 'Jenias', gender:'F', photoID: '002', school: 'Guelph University', schoolType: 'University', program: 'Plant Science', degree: 'Master', current: 'Studing', nSuccessCases: 2 },
	                       {name: 'Vivian', gender:'F', photoID: '003', school: 'Guelph University', schoolType: 'University', program: 'Animal Science', degree: 'Master', current: 'Studing', nSuccessCases: 2 }];

	var layout = {page:{w:800}};
	
    $scope.multiPages = {
            opt: {
                w: layout.page.w,
                steps: [{ id: 1, title: "Choose Consultant", validate: null },
                        { id: 2, title: "Upload Document", validate: null },
                        { id: 3, title: "Pay Fee" }
                ]
            },
            selected: { index: 0 }
        };
    
    var params = $location.search();
    
    if(params.hasOwnProperty('PayerID')){
    	$scope.multiPages.selected = {index:2};
    	$scope.bConfirm = true;
    }else{
    	$scope.bConfirm = false;
    }
    
    $scope.user = $ms.getSessionItem('user');
    
    if($scope.user){
    	$scope.ps = {title:'My Letter', username:$scope.user.username};
    }else{
    	$scope.ps = {title:'My Letter', username:''};
    }
}]);