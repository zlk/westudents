'use strict';

//---------------------------------------------------------------------------------------------------------------
// Home controller
//---------------------------------------------------------------------------------------------------------------
angular.module('mainModule')

.controller('PlanController', [ '$scope', '$rootScope', '$http', '$location', 'MainService',
		  function( $scope, $rootScope, $http, $location, $ms) {
		var layout = {page:{w:800}};
	
		$scope.multiPages = {
            opt: {
                w: layout.page.w,
                steps: [{ id: 1, title: "Form", validate: null },
                        { id: 2, title: "Plan", validate: null }
                ]
            },
            selected: { index: 0 }
        };
	
		$scope.planYears = ['May 2016', 'Nov 2016', 'Jan 2017'];
		
		$scope.majors = [{id:0, title:'Computer Science'},
							{id:1, title:'Chemistry'},
							{id:2, title: 'Animal'},
							{id:2, title: 'Plant'},
							{id:3, title: 'Others'}];

		$scope.currEnrolls = [{id:0, title:'University Grade 4'},
		                         {id:1, title:'University Grade 3'},
		                         {id:2, title:'College Grade 3'},
		                         {id:3, title:'College Grade 2'},
		                         {id:4, title:'Postgraduated and Work'},
		                         {id:5, title:'Postgraduated Studing'},
		                         {id:6, title:'University graduated and Work'},
		                         {id:7, title:'College graduated more than 3 years'},
		                         {id:8, title:'College graduated less than 3 years'}];//Currently enrolled in grades
		

		
		$scope.types = [{id:0, title:'Research Master'},
		               {id:1, title:'Taught Master'}];
		
		$scope.timeline = [{id:0, title:'Self Evaluation', description:"My Chance", icon:"check", direction:"inverted"},
		                    {id:0, title:'IELTS Program', description:"Take a course to learn IELTS, and improve english", icon:"credit-card", direction:"inverted"},
							{id:0, title:'Course Test', description:"Improve my Course performance", icon:"floppy-disk", direction:"inverted"},
							{id:0, title:'Choose Program', description:"Choose School and program", icon:"thumbs-up", direction:"inverted"},
							{id:0, title:'Prepare Application', description:"Personal Statement, Reference Letters, GPA, Certificate and Schoolarship", icon:"check", direction:"inverted"},
							{id:0, title:'Take Offer', description:"Fill Application Form, Transcripts, Pay application fee and Mail", icon:"check", direction:"inverted"},
							{id:0, title:'Apply Visa', description:"Fill Form, Bank statement and Visa", icon:"check", direction:"inverted"},
							{id:0, title:'Before Leave', description:"Air Ticket, Airport pickup, Apartment or home stay", icon:"check", direction:"inverted"}];
		
		
		$scope.tasks = [{id:1, parent:0, level:0, title:'Self Evaluation', start:'1/1/2016', end:'2/1/2016', description:"Take a course to learn IELTS, and improve english"},
		                {id:9, parent:1, level:1, title:'Evaluate Fund', start:'1/1/2016', end:'1/5/2016', description:"Take a course to learn IELTS, and improve english"},
		                {id:10,parent:1, level:1, title:'Evaluate Acdamic score', start:'1/3/2016', end:'1/8/2016', description:"Take a course to learn IELTS, and improve english"},
		                {id:2, parent:0, level:0, title:'IELTS Program', start:'2/15/2016', end:'5/1/2016', description:"Take a course to learn IELTS, and improve english"},
		                {id:11,parent:2, level:1, title:'Take IELTS Course', start:'2/15/2016', end:'3/10/2016', description:"Take a course to learn IELTS, and improve english"},
		                {id:12,parent:2, level:1, title:'Apply IELTS Exam', start:'3/10/2016', end:'3/11/2016', description:"Take a course to learn IELTS, and improve english"},
		                {id:13,parent:2, level:1, title:'Take IELTS Exam', start:'3/29/2016', end:'3/30/2016', description:"Take a course to learn IELTS, and improve english"},
		                {id:3, parent:0, level:0, title:'Course Test', start:'4/1/2016', end:'9/1/2016', description:"Take a course to learn IELTS, and improve english"},
						{id:4, parent:0, level:0, title:'Choose Program', start:'6/1/2016', end:'11/1/2016', description:"Take a course to learn IELTS, and improve english"},
						{id:5, parent:0, level:0, title:'Prepare Application', start:'9/1/2016', end:'12/1/2016', description:"Take a course to learn IELTS, and improve english"},
						{id:6, parent:0, level:0, title:'Take Offer', start:'6/1/2016', end:'11/1/2016', description:"Take a course to learn IELTS, and improve english"},
						{id:7, parent:0, level:0, title:'Apply Visa', start:'6/1/2016', end:'11/1/2016', description:"Take a course to learn IELTS, and improve english"},
						{id:8, parent:0, level:0, title:'Before Leave', start:'6/1/2016', end:'11/1/2016', description:"Take a course to learn IELTS, and improve english"}];

		
		//$scope.bSchedule = false;
		//$scope.bForm = true;
		$scope.item = {planYear:'', major:'', type: '', currEnroll:''};
		
		$scope.addBtn = {
			'id': 'add-plan-menu',
			'type':'dropdown',
			'text': 'Add Plan',
			'data':['Add Above','Add Below', 'Add as Child']
		};
		
		
		$scope.openSchoolSelect = function(){
			$rootScope.$broadcast('OnShowSchoolSelect');
		}
		
		$scope.getImage = function(imageID){
			return '../images/schools/' + imageID + '.png';
		}
		
		
		$scope.showSchedule = function(){
			//$scope.bSchedule = true;
			//$scope.bForm = false;
			$scope.multiPages.selected = {index:1};
		}
		
		$scope.savePlan = function(){
			$scope.user = $ms.getSessionItem('user');
			if($scope.user){
			
			}else{
				$rootScope.$broadcast('OnShowSignup');
			}
		}
		
		$ms.resize();
	
			$scope.bMobile = false;//MobileService.isMobile();
			$scope.showHome = true;
			$scope.showPropertyDetail = false;
		
			$scope.placeholderVal = "School";
			
			$scope.bSearchIcon = true;
			
			var SearchType = 0; //SearchService.SearchType;
			var ItemStatus = 0; //PropertyService.ItemStatus;
			
			$scope.signup = {username:'',password:'',email:'', role:'1', usernameMsg :"", passwordMsg:"", emailMsg:"", agreementMsg:""};
			
			$scope.signupTitle = "Sign up to post free Ads today";
			//$scope.seachboxTitle = "Find a rental housing in Canada";
			
			//$scope.seachboxTitle = gettextCatalog.getString("Find a rental housing in Canada");

			//----------------------------------------------------------------------------------
			// Public methods
			//----------------------------------------------------------------------------------
			$scope.openSignUpPage = function() {
				$location.path('signup');
			}
			
			$scope.redirectTo = function(a){
				$location.path(a);
			}
			
			//--------------------------------------------------------------------
			// When navigate to the home page, init() will be called to 
			// 1. load filter from the cookie
			// 2. send http request to get the item list and save
			//--------------------------------------------------------------------
			function init(){
				$scope.schools = [];
				$ms.getSchools(function(d){
					$.each(d.schools, function(i,v){
						if(["University of Toronto", "University of Guelph", "Wilfrid Laurier University","University of Waterloo","University of Western Ontario"].indexOf(v.name)!=-1){
							$scope.schools.push(v);
						}
					});
				});
			}
			
			init();
			
		} ])
