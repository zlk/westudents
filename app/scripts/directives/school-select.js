'use strict';

angular.module('wsui')

.directive('schoolSelect', function () {
    return {
        restrict: 'E',
        scope: {
//            opt: '=',
            schools: '='
        },
        transclude:true,
        templateUrl: '/scripts/directives/school-select.html',
        link: function (scope, element, attrs) {
			scope.programs = [{id: 0, name:'Animal Science', school: 'Guelph University'},
			                    {id: 1, name:'Plant Science', school: 'Guelph University'},
			                    {id: 2, name:'Computer Science', school: 'Conestoga College'},
			                    {id: 3, name: 'Business Accounting', school:'Toronto University'}];
        	scope.opt = {title:'Select School'};
        	scope.error = {username:'', password:'', email:''};
        	scope.data = {username:'', password:'', email:'',agree:''};
        	scope.$on('OnShowSchoolSelect', function(e, args){
	            var $modal = $(element).find('.w-popup');
	            var $content = $modal.find('.modal-content');
	            
	            $content.width(800).height(600);
	
	            //$modal.find('.modal-title').text('Sign Up');
	  
	
	            //$modal.find('.modal-body p').text(opt.body);
	            //$modal.find('.modal-body p').text('Hi');
	            $modal.modal({
	                backdrop: true,
	                keyboard: true
	            });
	            
	            $modal.css('margin-left', $(window).width()/2 - $modal.find('.modal-content').width()/2 + 'px');
	            $modal.css('margin-top', $(window).height()/2 - $modal.find('.modal-content').height()/2 + 'px');
	            
	            $modal.find('.btn-yes').hide();
	            $modal.find('.btn-no').hide();
	            $modal.find('.btn-ok').hide();
        	});
        }// end of link
    }//end of return
});