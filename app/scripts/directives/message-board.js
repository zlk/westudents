'use strict';

angular.module('wsui')

.directive('zMessageBoard', function () {
    return {
        restrict: 'E',
        scope: {
            opt: '=',
            data: '=',
            msg: '=',
            onSend:'&'
        },
        //transclude:true,
        templateUrl: '/scripts/directives/message-board.html',
        link: function (scope, element, attrs) {
            
        	scope.submit = function () {
                if (scope.onSend){
                    scope.onSend();
                }
            }

        	scope.getDirection = function(m){
        		return scope.opt.from.username == m.from ? 'l' : 'r';
        	}
        	
        	scope.getPhoto = function(m){
        		return scope.opt.from.username == m.from ? scope.opt.from.photo : scope.opt.to.photo;
        	}
            //scope.$watch('msg.text', function (newV, oldV) {

            //});
        }// end of link
    }//end of return
});
