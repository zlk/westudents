'use strict';

angular.module('wsui')

.directive('dropdown', ['MainService', function ($ms) {
    return {
        restrict: 'E',
        scope: {
            opt: '=',
            data: '=',
            selected: '=',
            onSelect:'&'
        },
        templateUrl: '/scripts/directives/dropdown.html',
        link: function (scope, element, attrs) {

            var $dropdown = $(element).find('.z-dropdown');
       
            $dropdown.change(function(e){
            	if(scope.onSelect){
            		var query = {};
            		query[scope.opt.valueField] = scope.selected.val;
            		var a = $ms.findWhere(scope.data, query);
            		if(a){
            			scope.selected.text = a[scope.opt.textField];
            		}
            		
            		scope.onSelect({item: a});
            	}
            });
        }// end of link
    }
}])
