﻿'use strict';

angular.module('wsui')

.directive('multiSteps', function () {
    return {
        restrict: 'E',
        scope: {
            //onResize: '&',
            opt: '=',
            selected: '='
        },
        transclude: true,
        template: '<div class="row w-row z-wizard" ng-transclude></div>',
        link: function (scope, element, attrs) {
            
            var $container = $(element).find('.multi-steps');
            var $pagerHolder = $container.find('.w-pager-holder');
            var steps = scope.opt.steps;
            var n = steps.length ? steps.length : 0;
            var self = scope;

            if (steps && n) {
                /*$.each(steps, function (i, step) {
                    $pagerHolder.append('<li class="w-pager" data-index="' + step.id + '"><span></span><div class="step-title" translate>' + step.title + '</div></li>');
                });*/
                var gap = scope.opt.w / n + 40;
                $pagerHolder.find('li').width((scope.opt.w - 40) / n);

                $.each($pagerHolder.find('li span'), function (i, v) {
                    if (i != 0 && i != n) {
                        $(v).css('left', '-' + (Math.floor((scope.opt.w - gap) / (n-1) / 2)) + 'px');
                    }
                })

                //patch for ie 11
                $pagerHolder.find('[data-index="1"] span').hide();
            }

            var $pagers = $pagerHolder.find('li.w-pager');

            //-------------------------------------------------
            // Date:    2015/06/21
            //
            scope.initPagers = function () {
            	var index = scope.selected.index;
                scope.showPager(index);
            }

            //-------------------------------------------------
            // Argument:
            //  index --- index of the page, start from 0
            scope.showPager = function (index) {
                $.each($pagers, function (i, v) {
                    if (i <= index) {
                        $(v).addClass('active');
                        $(v).find('span').addClass('active');
                    } else {
                        $(v).removeClass('active');
                        $(v).find('span').removeClass('active');
                    }

                    if (i == index) {
                        $(v).addClass('selected');
                    } else {
                        $(v).removeClass('selected');
                    }
                });
            }

            //-------------------------------------------------
            // Argument:
            //  index --- index of the page, start from 0
            scope.nextPager = function (index) {
                if (index < $pagers.length - 1) {
                    scope.showPager(index + 1);
                }
            }

            //-------------------------------------------------
            // Argument:
            //  index --- index of the page, start from 0
            scope.prevPager = function (index) {
                if (index > 0) {
                    scope.showPager(index - 1);
                }
            }


            var $pages = $container.find('.w-page');

            //-------------------------------------------------
            // Arguments:
            //  id --- current page id
            scope.init = function () {
            	var index = scope.selected.index;
                $($pages[index]).addClass('active');

                // Show default page
                $.each($pages, function (i, v) {
                    if (scope.isActive($(v))) {
                        $(v).show();
                    } else {
                        $(v).hide();
                    }
                });
            }

            //-------------------------------------------------
            // Arguments:
            //  id --- current page id
            scope.isActive = function ($page) {
                return $page.hasClass('active');
            }

            //-------------------------------------------------
            // Argument:
            //  index --- index of the page, start from 0
            scope.next = function (index) {
                if (index < $pages.length - 1) {
                    scope.showPage(index + 1);
                    scope.selected = { 'index': index + 1 };
                }
            }

            //-------------------------------------------------
            // Argument:
            //  index --- index of the page, start from 0
            scope.prev = function (index) {
                if (index > 0) {
                    scope.showPage(index - 1);
                    scope.selected = { 'index': index - 1 };
                }
            }

            //-------------------------------------------------
            // Argument:
            //  index --- index of the page, start from 0
            scope.showPage = function (index) {
                $.each($pages, function (i, v) {
                    if (index == i) {
                        $(v).addClass('active');
                        $(v).show();
                    } else {
                        $(v).removeClass('active');
                        $(v).hide();
                    }
                });
            }

            function validatePrevPages(pageIndex) {
                var bPass = true;
                var i = 0;
                for (i = 0; i < pageIndex; i++) {
                    var validateFunc = steps[i].validate;
                    if (validateFunc) {
                        if (!validateFunc()) {
                            bPass = false;
                            break;
                        }
                    }
                }
                return { result: bPass, pageIndex: i };
            }

            // Wizard pager click handler
            $.each($pagers, function (k, v) {
                $(v).bind('click', function (e) {
                    //if ($(e.currentTarget).hasClass('disabled')) {
                    //    e.preventDefault();
                    //    if (e.stopImmediatePropagation) {
                    //        e.stopImmediatePropagation();
                    //    } else {
                    //        e.stopPropagation();
                    //    }
                    //} else {
                    //var id = $(this).attr('id');
                    //var index = parseInt(id.replace(/w-pager-/g, '')) - 1;
                    var index = parseInt($(this).data('index')) - 1;

                    var r = validatePrevPages(index);
                    if (r.result) {
                        scope.showPage(index);
                        scope.showPager(index);
                        scope.selected = { 'index': index };
                    } else {
                        scope.showPage(r.pageIndex);
                        scope.showPager(r.pageIndex);
                        scope.selected = { 'index': r.pageIndex };
                    }
                    //}
                });
            });

            // wizard button click handler
            $.each($container.find('.btn-next'), function (k, v) {
                $(v).click(function (e) {
                    //var data = e.currentTarget.dataset;
                    //var pageID = data.pageid;
                    //var pageID = $(this).closest('.w-page').attr('id');
                    //var index = parseInt(pageID.replace(/ws-/g, '')) - 1;

                    var index = $(this).closest('.w-page').data('index') - 1;

                    if (index < steps.length - 1) {
                        var validateFunc = steps[index].validate;
                        if (validateFunc) {
                            if (validateFunc()) {
                                scope.next(index);
                                scope.nextPager(index);
                            } else {
                                if (index == 0) {
                                    //_showAlertDialog({ body: 'Select a County, a Municipality or an FSA.' });
                                }
                            }
                        } else {
                            scope.next(index);
                            scope.nextPager(index);
                        }
                    }
                });
            });

            $container.find('.btn-prev').click(function (e) {
                //var pageID = $(this).closest('.w-page').attr('id');
                //var index = parseInt(pageID.replace(/ws-/g, '')) - 1;
                var index = $(this).closest('.w-page').data('index') - 1;

                scope.prev(index);
                scope.prevPager(index);
            });


            scope.$watch("selected", function (newVal, oldVal) {
                if (scope.selected) {
                    var index = parseInt(scope.selected.index);
                    scope.showPager(index);
                    scope.showPage(index);
                }
            });

            scope.initPagers();
            scope.init();


        }// end of link
    }
})