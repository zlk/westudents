'use strict';

angular.module('wsui')

.directive('imagePreview', [function () {
    return {
        restrict: 'E',
        scope: {
            opt: '=',
            data: '=',
            onSelect:'&'
        },
        transclude:true,
        template: '<div class="image-preview"><canvas class="preview-canvas"></canvas></div>',
        link: function (scope, element, attrs) {
        	var $list = $(element).find('.image-preview');
        	
        	var $preview;
			var $previewImage;
			var DEFAULT_PREVIEW_WIDTH = 560;
			var img = new Image();
			var previewCanvas;
			var bImageLoaded = false;
			
			var canvas = {width:80, height:80};
			

			
			
			//----------------------------------------------------------------------------------------------
			//	Public methods
			//----------------------------------------------------------------------------------------------

			scope.getImageSrc = function(image){
				if(image){
					var dt = new Date();
					if(image.fpath!=''){
						return image.fpath + '?' + dt.valueOf().toString();
					}else if(image.data!=''){
						return image.data;
					}else{
						return null;
					}
				}else{
					return null;
				}
			}
						
			//-------------------------------------------------------------------------
			//	Calculate resized image size
			//		image --- must have width and height attribute before call it.
			//		canvas --- jquery object
			// if image height or width exceeds max, do resize, other wise not
			// canvas size is fixed
			//-------------------------------------------------------------------------
			scope.adjustImageSize = function(image, $canvas){
				var maxWidth = $canvas.width();
				var maxHeight = $canvas.height();
				var w = image.width;
				var h = image.height;
				
				if(h > maxWidth || w > maxHeight){
					var ratioH = h / maxHeight;
					var ratioW = w / maxWidth;
					var ratio = ratioW > ratioH ? ratioW : ratioH;
					
					h = image.height/ratio;
					w = image.width/ratio;
				}
				
				return { width: w, height: h };
			}
			
			scope.rotate = function(){
			
			}
			
			scope.change = function(){	
			
			}
			

			//----------------------------------------------------------------------------------------------
			// If didn't click submit button, only need to clean the scope.images.
			// Otherwise need to send back to delete picture on server.
			//----------------------------------------------------------------------------------------------
			scope.remove = function(){
				if(!ImageUploaderService.isAllEmpty(scope.images)){
					op = OPERATION_MODE.REMOVE;
					var index = ImageUploaderService.getSelectedIndex(scope.images);
					var _id = scope.images[index].id;
					ImageUploaderService.removeImage(index, scope.images);
					scope.bArrow = !ImageUploaderService.isAllEmpty(scope.images);
					
					
					// Update preview image
					if(ImageUploaderService.isAllEmpty(scope.images)){
						// Preview set empty
						//var preview = elem.find('.preview-canvas')[0];
						//$(preview).attr('src', '');
						scope.selectedImage = {id:'', order:0, fpath:'', data:'', active:false, selected:true, width:360};
					}else{
						/*
						// Preview selected image
						var selectedIndex = ImageUploaderService.getSelectedIndex(scope.images);
						//previewImage(scope.images[selectedIndex]);
						var PREVIEW_WIDTH = MobileService.isMobile() ? MobileService.getDeviceWidth() : DEFAULT_PREVIEW_WIDTH;
						var PREVIEW_HEIGHT = PictureService.getFrameHeight( PREVIEW_WIDTH );
						PictureService.adjustPreview(scope.images[selectedIndex], $previewImage, PREVIEW_WIDTH, PREVIEW_HEIGHT);
						*/
					}
					scope.$apply(function () {});
				}
				
				if(_id!=null && _id!= '' && _id != 0){
					if(scope.$parent.removePictureCallback){
						scope.$parent.removePictureCallback(_id);
					}	
				}
				
				//PictureService.setPicture(scope.image);
			}

			//----------------------------------------------------------------------------------------------
			// Events
			//----------------------------------------------------------------------------------------------
			
			//----------------------------------------------------------------------------------------------
			// This event will be triggered by 2 cases, when assign img.src:
			//		1. select the item
			//		2. open file dialog and select by reader.onload img.src = e.target.result;
			// Load image by selecting from file dialog, Change image array after add new image from file dialog
			//----------------------------------------------------------------------------------------------
			img.onload = function(){
				   var newSize = ImageUploaderService.adjustImageSize(img, $(previewCanvas));
				   
				   var ctx = previewCanvas.getContext("2d");
				   ctx.restore();
				   bImageLoaded = false;
				   
				   // Resize and redraw the preview in the middle
				   var _data = previewImage(img);
				   
				   var _pic = {id:'', order:targetIndex, fpath:'', data:_data, active:true, selected:true, 
						   width: newSize.width, height:newSize.height };
				   
				   if(op == OPERATION_MODE.ADD){
					   // Redraw the thumbnail
					   ImageUploaderService.addImage(targetIndex, _pic, scope.images);
					   scope.bArrow = !ImageUploaderService.isAllEmpty(scope.images);
				   }else if(op == OPERATION_MODE.CHANGE){
					   _pic.id = scope.images[targetIndex].id;
					   // Redraw the thumbnail
					   ImageUploaderService.changeImage(targetIndex, _pic, scope.images);   
				   }
				   op = OPERATION_MODE.NONE;
				   // Rebuild DOM to update the thumbnail list for adding image
				   scope.$apply(function () {});
			};
			
			//----------------------------------------------------------------------------------------------
			// This event will be triggered by 2 cases:
			//		1. select the item
			//		2. open file dialog and select
			// call back for the canvas
			//----------------------------------------------------------------------------------------------
			/*img.addEventListener("load", function() {
				
				if(!imageLoaded){
					// Redraw the preview
					var data = previewImage(img);
				}

				// Change image array after add new image from file dialog
				var index = ImageUploaderService.getSelectedIndex(scope.images);
				scope.images[index].data = data;
				
				// Rebuild DOM to update the thumbnail list for adding image
				scope.$apply(function () {});
				
			}, false);*/
			
			
			scope.$watch('selectedImage',function(){
				
				if(scope.selectedImage){
					var image = scope.selectedImage;
					
					// Update preview frame and preview size
					$previewImage = $('.preview-canvas');
					ImageUploaderService.changeImage(image.order, image, scope.images);
					/*
					var PREVIEW_WIDTH = MobileService.isMobile() ? MobileService.getDeviceWidth() : DEFAULT_PREVIEW_WIDTH;
					var PREVIEW_HEIGHT = PictureService.getFrameHeight( PREVIEW_WIDTH );
					PictureService.adjustPreview(image, $previewImage, PREVIEW_WIDTH, PREVIEW_HEIGHT);
					scope.frameStyle = PictureService.getFrameStyle(image, PREVIEW_WIDTH, PREVIEW_HEIGHT);
					*/
					
					bImageLoaded = false;
					var ctx = previewCanvas.getContext("2d");
					ctx.restore();
					// Trigger 'load' event to redraw the preview
					img.src = scope.getImageSrc(scope.selectedImage);
				}
			});
			
			//----------------------------------------------------------------------------------------------
			// Private methods
			//----------------------------------------------------------------------------------------------
			
			
			
			//-----------------------------------------------------------------------------------------------
			//	For Mobile canvas width is device width, For Desktop canvas width is default width
			// Arguments:
			//		canvas --- canvas object
			//-----------------------------------------------------------------------------------------------
			function setCanvasSize(canvas){
				
				var w = MobileService.isMobile() ? MobileService.getDeviceWidth() : DEFAULT_PREVIEW_WIDTH;
				var h = PictureService.getFrameHeight( w );
				
				canvas.width = w;
				canvas.height = h;
				
				// Must set attribute here, for prevent scale in Chrome and Firefox
				$(canvas).attr('width', w).attr('height', h);
			}
			
			//-------------------------------------------------------------------------------------------------
			//	Set up canvas size and show image
			//-------------------------------------------------------------------------------------------------
			function init(){
				previewCanvas = elem.find('.preview-canvas')[0];
				
				setCanvasSize(previewCanvas);
				
				// Show selected image
				if(scope.selectedImage){
					scope.onSelect(scope.selectedImage);
				}else{
					// Show the first image
					if(scope.images){
						scope.onSelect(scope.images[0]);
					}else{
						// Clean up the canvas
						if(previewCanvas){
							var ctx = previewCanvas.getContext("2d");
							ctx.clearRect ( 0 , 0 , previewCanvas.width, previewCanvas.height );
						}
					}
				}
			}
			
			//---------------------------------------------------------------------------------------------------------------
			//	resized image and draw in the middle of canvas, will show the picture after call this function
			//	Arguments:
			//		image --- Image object, must have width and height attribute before call it.
			// 			if image height or width exceeds max, do resize, other wise do not resize.
			//	Return:
			//		image data URIs
			//---------------------------------------------------------------------------------------------------------------
			function previewImage(image){

				if(previewCanvas && previewCanvas.getContext && image.width!=0){
					
					var ctx = previewCanvas.getContext("2d");
					
					newSize = ImageUploaderService.adjustImageSize(image, $(previewCanvas));
					// Clear background
					ctx.clearRect(0, 0, previewCanvas.width, previewCanvas.height);
					
					if(!bImageLoaded){
						var dx = (previewCanvas.width - newSize.width)/2;
						var dy = (previewCanvas.height - newSize.height)/2;
						ctx.save();
						ctx.translate(dx, dy);
					}
					
					bImageLoaded = true;
					
					ctx.drawImage(image, 0, 0);
					
					// toDataURL returns a data URIs containing the image in the format specified by the 
					// type parameter (defaults to PNG). The returned image is in a resolution of 96 dpi.
					return previewCanvas.toDataURL('image/jpeg');
				}else{
					return null;
				}
			}

			//---------------------------------------------------------------------------------------------------
			//	rotate image in html canvas
			//	Arguments:
			//		image --- Image object, must be resized image
			//---------------------------------------------------------------------------------------------------
			function rotateImage(image, degree){
				var radians = degree * Math.PI / 180;
				
				if(previewCanvas && previewCanvas.getContext){
					
					var ctx = previewCanvas.getContext("2d");
					ctx.clearRect(0, 0, previewCanvas.width, previewCanvas.height);
					
					var dx2 = image.width / 2;
					var dy2 = image.height / 2;
					ctx.translate(dx2, dy2);
					ctx.rotate(radians);
					ctx.translate(-dx2,-dy2);
					ctx.drawImage(image, 0, 0);
					
					bImageLoaded = true;
					
					return previewCanvas.toDataURL('image/jpeg'); //image data
				}else{
					return null;
				}
			}
			
			//---------------------------------------------------
			// Init
			//---------------------------------------------------

        }
	}
}])