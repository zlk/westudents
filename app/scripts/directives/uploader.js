'use strict';

angular.module('wsui')

.directive('uploader', function () {
    return {
        restrict: 'E',
        scope: {
            opt: '=',
            onComplete: '&',
            onBefore: '&'
        },
        templateUrl: '/scripts/directives/uploader.html',
        link: function (scope, element, attrs) {
        	
        	var $container = $(element).find('.uploader');
            var d = scope.data;
            var reader = new FileReader();
            var _data;

			//----------------------------------------------------------------------
			// Upload object and  its call backs 
			//----------------------------------------------------------------------
			var oReq = new XMLHttpRequest();
			oReq.upload.addEventListener("progress", updateProgress, false);
			oReq.upload.addEventListener("load", transferComplete, false);
			oReq.upload.addEventListener("error", transferFailed, false);
			oReq.upload.addEventListener("abort", transferCanceled, false);
			
            scope.file = {name:''};

            var img = new Image();
			//-----------------------------------------------------------------------------------------------------
			// This event will be triggered by 2 cases, when assign img.src:
			//		1. select the item
			//		2. open file dialog and select
			// Load image by selecting from file dialog, Change image array after add new image from file dialog
			//-----------------------------------------------------------------------------------------------------
			img.onload = function(){
				scope.opt.size = scope.getDimension(img, 128, 96);
				scope.upload(img.src);
			}
			
			scope.getDimension = function(image, maxWidth, maxHeight){
				//var maxWidth = $canvas.width();
				//var maxHeight = $canvas.height();
				var w = image.width;
				var h = image.height;
				
				if(h > maxWidth || w > maxHeight){
					var ratioH = h / maxHeight;
					var ratioW = w / maxWidth;
					var ratio = ratioW > ratioH ? ratioW : ratioH;
					
					h = image.height/ratio;
					w = image.width/ratio;
				}
				
				return { width: w, height: h };
			}
            
            
            
            $container.find('.file-upload').click(function(){
            	
            	$container.find('.upload-input').click();
            	
            });
            
            function dataURItoBlob(dataURI) {
        	    // convert base64/URLEncoded data component to raw binary data held in a string
        	    var byteString;
        	    if (dataURI.split(',')[0].indexOf('base64') >= 0){
        	        byteString = atob(dataURI.split(',')[1]);
        	    }else{
        	        byteString = unescape(dataURI.split(',')[1]);
        	    }
        	    // separate out the mime component
        	    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

        	    // write the bytes of the string to a typed array
        	    var ia = new Uint8Array(byteString.length);
        	    for (var i = 0; i < byteString.length; i++) {
        	        ia[i] = byteString.charCodeAt(i);
        	    }

        	    return new Blob([ia], {type:mimeString});
        	}
            
			scope.onSelect = function(image){
				// Change image to be selected in the image array
				ImageUploaderService.changeImage(image.order, image, scope.images);
				
				// trigger 'selectedImage' event and update thumbnail list
				scope.selectedImage = image;
			}
			
								
			scope.fileChanged = function(element){
				var files = element.files;
				
				// ok button handler
				if (files && files[0]) {
					scope.file = {'name': files[0].name};
					reader.readAsDataURL(files[0]);
					scope.file.name = files[0].name;
				}else{
					// cancel button hander
					scope.file.name = '';
				}
			}
			
			//----------------------------------------------------------------------------------------------
			// This event will be called first when select the image from file dialog
			// call back for the file dialog
			//----------------------------------------------------------------------------------------------
			reader.onload = function(e) {
				// Trigger 'load' event for the canvas
				//img.src = e.target.result;
				//_data = e.target.result;
				if(scope.opt && scope.opt.username){
					
					if(scope.opt.type=='photo'){
						img.src = e.target.result;
					}else{
						if(scope.onBefore){
							if(scope.onBefore({'data':e.target.result})){
								scope.upload(e.target.result);
							}
						}
					}
				}
					
				//readerLoaded = true;
			}
			
			//----------------------------------------------------------------------------------------------
			// This event will be triggered by 2 cases, when assign img.src:
			//		1. select the item
			//		2. open file dialog and select
			// Load image by selecting from file dialog, Change image array after add new image from file dialog
			//----------------------------------------------------------------------------------------------
			/*img.onload = function(){
				   var newSize = ImageUploaderService.adjustImageSize(img, $(previewCanvas));
				   
				   var ctx = previewCanvas.getContext("2d");
				   ctx.restore();
				   bImageLoaded = false;
				   
				   // Resize and redraw the preview in the middle
			};*/
			
			//----------------------------------------------------------------------------------------------
			// This event will be triggered by 2 cases:
			//		1. select the item
			//		2. open file dialog and select
			// call back for the canvas
			//----------------------------------------------------------------------------------------------
			/*img.addEventListener("load", function() {
				
				if(!imageLoaded){
					// Redraw the preview
					var data = previewImage(img);
				}

				// Change image array after add new image from file dialog
				var index = ImageUploaderService.getSelectedIndex(scope.images);
				scope.images[index].data = data;
				
				// Rebuild DOM to update the thumbnail list for adding image
				scope.$apply(function () {});
				
			}, false);*/

			
			function transferComplete (e) {
				scope.progressVal = 100;
				scope.$apply(function () {
					scope.progressVal = 100;
				});
				
				if(scope.onComplete)
					scope.onComplete();
				console.log("transferComplete");
			}
			
			function updateProgress (e) {
			    var done = e.position || e.loaded, total = e.totalSize || e.total;
			    scope.$apply(function () {
			    	scope.progressVal = Math.floor(done/total*1000)/10;
				});
				console.log("updateProgress");
			}
			
			function transferFailed (e) {
				console.log("transferFailed");
			}
			
			function transferCanceled (e) {
				console.log("transferCanceled");
			}
			
			
			//
            scope.upload = function(d){
            	// Trigger 'load' event for the canvas
				//img.src = e.target.result;
            	
    			var formData = new FormData();
    			
    			if(scope.file.name){
    				var blob = dataURItoBlob(d);
    				var info = {type: scope.opt.type, fname: scope.file.name, username: scope.opt.username};
    				
    				if(scope.opt.type == 'photo'){
    					info.width = scope.opt.size.width;
    					info.height = scope.opt.size.height;
    				}
    				
        			formData.append('info', JSON.stringify(info));
    				formData.append('fdata', blob);
        			
        			// ----------------------------------------------------------------------
        			// void open(DOMString method, DOMString url, optional boolean async, 
        			//		optional DOMString? user, optional DOMString? password)
        			// ----------------------------------------------------------------------
        			oReq.open("post", "/api/upload", true);
    				
        			// ----------------------------------------------------------------------
        			// You must call setRequestHeader() after open(), but before send(). 
        			// If this method is called several times with the same header, the 
        			// values are merged into one single request header.
        			// ----------------------------------------------------------------------
        			//oReq.setRequestHeader('Content-Type', "multipart/form-data;boundry:xxxx");
        			
        			// ----------------------------------------------------------------------
        			// void send(FormData data);
        			// ----------------------------------------------------------------------
        		    oReq.send(formData);

    			}
    			
    			//var fullPath = document.getElementById('upload-input').value;
    			
            }
            
            
			

        }
    }
})

						