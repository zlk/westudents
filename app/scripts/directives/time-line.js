'use strict';

angular.module('wsui')

.directive('timeline', function () {
    return {
        restrict: 'E',
        scope: {
            //opt: '=',
            data: '='
        },
        templateUrl: '/scripts/directives/time-line.html',
        link: function ($scope, element, attrs) {
            var d = $scope.data;
        }
    }
})
