'use strict';

angular.module('wsui')

.directive('gantt', ['$timeout', function ($timeout) {
    return {
        restrict: 'E',
        scope: {
            //opt: '=',
            data: '='
        },
        templateUrl: '/scripts/directives/gantt.html',
        link: function (scope, element, attrs) {
        	
        	var $table = $(element).find('.gantt');
        	var WIDTH = 12 * $table.find('td.interval').width();
        	var LINE_HEIGHT = 21;
        	
        	scope.select = function(t){
        		$(element).find('.task-table tr').css('background-color', '#fff');
        		$(element).find('.row-'+t.id).css('background-color', 'rgb(152, 200, 234)');
        	} 
        	
        	scope.getDate = function(str){
        	    var mdy = str.split('/')
        	    return new Date(mdy[2], mdy[0]-1, mdy[1]);
        	}
        	
        	scope.getDays = function(start, end){
        		var s = scope.getDate(start);
        		var e = scope.getDate(end);
        		
        		return Math.round((e-s)/(1000*60*60*24));
        	}
        	
        	scope.drawItem = function(i){
        		var t = scope.data[i];
        		var offset = scope.getDays('1/1/2016', t.start);
        		var len = scope.getDays(t.start, t.end);
        		var w = 12 * ($(element).find('td.interval').width()+2); // 2 for border
        		var padding = Math.floor(offset * w / 365);
        		var length = Math.floor(len * w / 365 );
        		
        		$(element).find('.item-wrapper-'+t.id).css({ 'padding-left':padding+'px', 'width':w+'px' });
        		$(element).find('.item-'+t.id).css({'width':length+'px', 'background-color': '#337ab7'});
        		
        		
        		$(element).find('.row-'+t.id).find('td.title').css({'padding-left':10 + 12 * t.level + 'px'});
        	}
        	
        	//init
        	scope.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];//[1,2,3,4,5,6,7,8,9,10,11,12];
        	$timeout(function(){
    			var w = Math.floor(WIDTH / 12);
    			var h = scope.data.length * LINE_HEIGHT;
        		for(var i=0; i<12; i++){
        			$(element).find('.month').css({'float':'left', 'height':h+'px', 'width':w+'px', 
        				'border-left': '1px solid #000000'});
        		}
        		$(element).find('.month:eq(12)').css({'border-right': '1px solid #000000'});
        		
	        	for(var i=0; i<scope.data.length; i++){
	        		scope.drawItem(i);
	        	}
	        	
	        	scope.select(scope.data[0]);
        	},0);
        	
        }
    }
}])
