'use strict';

angular.module('wsui')

.directive('list', ['$timeout', function ($timeout) {
    return {
        restrict: 'E',
        scope: {
            opt: '=',
            data: '=',
            onSelect:'&'
        },
        transclude:true,
        templateUrl: '/scripts/directives/list.html',
        link: function (scope, element, attrs) {
        	var $list = $(element).find('.list');
        	
        	scope.select = function(t){
        		$list.find('li').css('background-color', '#fff');
        		$list.find('li.row-'+t[scope.opt.valueField]).css('background-color', 'rgb(152, 200, 234)');
        	
        		if(scope.onSelect)
        			scope.onSelect({item: t});
        	} 
        }
	}
}])