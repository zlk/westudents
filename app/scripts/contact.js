angular.module('mainModule')

.controller('ContactController',
		[ '$http', '$scope', '$rootScope', 'MapService', '$location', 'MainService',
		  function($http, $scope, $rootScope, $map, $location,  $ms ) {

			$ms.resize();
			
			$scope.init = function() {
		    	//MapService.drawMap({id: 'contact-map-canvas', marker:{title:'Westudents.ca', address:'1087 Foxcreek Rd, London, ON, Canada'}});
			}
			
			function postFeedback(fb, successCb, errorCb){
				$http.post(cfg.apiUrl + '/postFeedback', fb)
					.success(function(data, status, headers, config) {
						if(successCb){
							successCb(data, status, headers, config);
						}
					})
					.error(function(){
						if(errorCb)
							errorCb();
					});
			}

			$scope.feedback = { email: '', body:'', date:''};
			
			$scope.postFeedback = function(){
				var dt = new Date();
				$scope.feedback.date = dt.toString();
				if($scope.feedback.email!='' && $scope.feedback.body!=''){
					postFeedback($scope.feedback, function(){
						alert('Post Feedback successful! \nThank you! We will read your message and write back soon.')
					})
				}else{
					alert('Email and message cannot be empty.')
				}
			}
		}]);