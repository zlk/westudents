
angular.module('mainModule')

.controller('BookingController',
		[ '$http', '$scope', '$rootScope', '$location', 'MainService', '$window', 
		  function($http, $scope, $rootScope, $location, $ms, $window ) {

			function init(){
	            var params = $location.search();
	            $scope.title = '';
	            
	            if(params.hasOwnProperty('t') && params.t){
	            	$scope.title = params.t;
	            }
	            
	            $scope.user = $ms.getSessionItem('user');
	            
	            var username = '';
	            if($scope.user){
	            	username = $scope.user.username;
	            }else{
	            	$location.path('login');
	            }
	            
	            $scope.services = [{type:'Resume', title:'Resume', 'username':username, price:60, description:'A resume is a one or two page summary of a person’s life history, and it is also called CV (curriculum vitae) or biodata. It lists one’s education, professional experience, and information if required. The purpose of including a resume for a college application is to convince people that you have the required education, skills, and experience to be successful.'},
	                               {type:'Personal Statement', title:'Personal Statement', 'username':username, price:60, description:'The personal statement also known as Statement of Purpose, Letter of Intent, or even Autobiographic Statement, is among the most important elements in your application. It is your chance to tell the university admission officers who you are and what’s important to you. It is also your opportunity to tell these officers about your suitability for the program that you hope to study.'},
	                               {type:'Reference Letter', title:'Reference Letter', 'username':username, price:60, description:'As one of the most important elements in your application, letters of recommendation provide evaluation of the applicant’s capability and qualities from qualified recommenders. An applicant is generally requested to submit two or three letters of recommendation. An effective recommendation letter is provided by the person who receives certain recognition in corresponding field and has a good understanding of your academic history, interests, goals, and direction.'}];
			
	          
				
	            $scope.cart = [];
	            
				var layout = {page:{w:800}};
	            
	            var params = $location.search();
	            
	            if(params.hasOwnProperty('PayerID')){
	            	$scope.bConfirm = true;
	            }else{
	            	$scope.bConfirm = false;
	            }
	            
				// show documents
				$scope.token = $ms.getSessionItem('token');
				$scope.docs = {data:[]};
				loadDocs();
				
			}
			
			init();
			

			
			$scope.onSelectSchool = function(text){
				var query = $ms.getSessionItem('consultantFilter');
				if(query){
					if(text!=''){
						query['school'] = text;
					}else{
						if(query.hasOwnProperty('school'))
							delete query.school;
					}
				}else{
					if(text!=''){
						query = {school:text};
					}else{
						query = {};
					}
				}

				$ms.setSessionItem('consultantFilter', query);
				
				$ms.getConsultants(query, function(d){
					$scope.consultants = d.users;
				});
			}
			
			$scope.onSelectProgram = function(text){
				var query = $ms.getSessionItem('consultantFilter');
				if(query){
					if(text!=''){
						query['program'] = text;
					}else{
						if(query.hasOwnProperty('program'))
							delete query.program;
					}
				}else{
					if(text!=''){
						query = {program:text};
					}else{
						query = {};
					}
				}

				$ms.setSessionItem('consultantFilter', query);
				$ms.getConsultants(query, function(d){
					$scope.consultants = d.users;
				});
			}
			
			$scope.resetFilter = function(){
				var query = {};
				$scope.programs.selected = '';
				$scope.schools.selected = '';
				
				$ms.setSessionItem('consultantFilter', query);
				$ms.getConsultants(query, function(d){
					$scope.consultants = d.users;
				});
				
				$scope.selectedConsultant = null;
			}
			
			$scope.selectConsultant = function(c){
				if($scope.selectedConsultant){
//					var found = $scope.consultants.filter(function(el){
//						return el._id == $scope.selectedConsultant._id;
//					});
					
//					if(!found){
						$scope.consultants.push($scope.selectedConsultant);
//					}
				}
				
				$scope.consultants = $scope.consultants.filter(function(el){
					return el._id != c._id;
				});
				
				$scope.selectedConsultant = c;
			}
			

			
			$scope.getPhoto = function(photoID){
				if(photoID)
					return '../images/consultants/' + photoID + '.jpg';
				else
					return '';
			}
			

			
			function loadDocs(){
				$http.post(cfg.apiUrl + '/getDocs', {'token': $scope.token}).success(function (data) {
			        if(data.success){
			        	$scope.docs.data = data.docs;
			        	updateCart();
			        }
				}).error(function(){
					$scope.docs.data = [];
				});	
			}
			
			
			

			function findWhere(a, obj) {
				if (a) {
					for (var i = 0; i < a.length; i++) {
						var v = a[i];
						var matched = true;

						for (var key in obj) {
							if (!(key in v && v[key] == obj[key])) {
								matched = false;
								break;
							}
						}

						if (matched) {
							return v;
						}
					}
				}
				return null;
			}

			function updateCart(){
				$scope.cart = [];
				$.each($scope.docs.data, function(i, doc){
					var p = findWhere($scope.services, {type: doc.type});
					if(p){
						doc.price = p.price;
						//doc.currency = p.currency;
					}
					$scope.cart.push(doc);
				})
			}
			
			$scope.rmDoc = function(d){
				var opt = $.extend({'token': $scope.token}, d);
				$http.post(cfg.apiUrl + '/rmDoc', opt).success(function (data) {
					//if(data.success){
						//$scope.docs.data = data.docs;
						$scope.docs.data = $scope.docs.data.filter(function(el){
							return el.fname != d.fname || el.created != d.created;
						});
						
						updateCart();
					//}
				}).error(function(){
					$scope.docs.data = [];
				});
			}
			
            $scope.goPaypal = function(){
            	var total = $scope.getTotal();
            	
            	$ms.setSessionItem('total', total);
            	
            	$http.post(cfg.apiUrl + '/setCheckout', {amount: total})
            		.success(function(data, status) {
            			// When redirecting the buyer back to your website from paypal.com, PayPal appends the Express Checkout transaction token 
            			// and the unique PayPal buyer ID as GET parameters to your RETURN URL; these GET parameters are named token and PayerID.
							$window.location.href = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" + data.TOKEN;
						})
					.error(function(data, status, headers, config) {
							
						});
            	
            	//$scope.getExpressCheckout(function(data){
            	//	$location.url = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" + data.TOKEN;
            	//})
            }
            
			
			
			$scope.getTotal = function(){
				var total = 0;
				$.each($scope.cart, function(i, v){
					total += v.price;
				})
				return total;
			}
			

			
			// confirm checkout
			$scope.confirm = function(){
	            var params = $location.search();
	            if(params.hasOwnProperty('PayerID')){
	            	var total = $ms.getSessionItem('total');
	            	var payerID = params.PayerID;
	            	var token = params.token;
	            	
	            	$http.post(cfg.apiUrl + '/checkout', {'amount': total,
	            		'token': token,
	            		'payerID': payerID})
            		.success(function(data, status) {
            			$ms.showSplashDialog({body:'Payment successful!'});
            		})
					.error(function(data, status, headers, config) {
							
					});
	            }
			}
			
			$scope.onCompleteUpload = function(){
				loadDocs();
			}
			
			$scope.onBeforeUpload = function(item){
				if(!($scope.user && $scope.user.username)){
					$location.path('login');
					return false;
				}else{
					return true;
				}
			}
			
			
			
			
		}]);