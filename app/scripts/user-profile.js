//----------------------------------------------------
// Author:	Martin.Zhang
// Date:  	December 29 2015
// All right reserved.
//----------------------------------------------------
'use strict';

angular.module('mainModule')

.controller('UserProfileController',
		[ '$http', '$scope', '$rootScope', '$location', 'MainService', function($http, $scope, $rootScope, $location, $ms ) {
			
			function init(){
	            $scope.user = $ms.getSessionItem('user');
	            
	            var username = '';
	            if($scope.user){
	            	username = $scope.user.username;
	            }else{
	            	$location.path('login');
	            }
	            
				$scope.uploader = {type:'photo', title:'Photo', 'username':username};
				$scope.photo = {src:''};
				
				$http.post(cfg.apiUrl + '/getUserPhoto', {'username': $scope.user.username})
				.success(function (data) {
					$scope.photo.src = data.success ? '../' + data.photo : '';
				}).error(function(){
					$scope.photo.src = '';
				});
			}
			
			init();
			
			$scope.onCompleteUpload = function(){
				$http.post(cfg.apiUrl + '/getUserPhoto', {'username': $scope.user.username})
					.success(function (data) {
						$scope.photo.src = data.success ? '../' + data.photo : '';
						$scope.user.photo = $scope.photo.src;
						$ms.setSessionItem('user', $scope.user );
						$rootScope.$broadcast('onUpdateHeader');
						setTimeout(function(){
							$scope.$apply();
						}, 0);
					}).error(function(){
						$scope.photo.src = '';//fix me
						setTimeout(function(){
							$scope.$apply();
						}, 0);
					});
			}
			
			$scope.onBeforeUpload = function(data){
				return true;
			}
			

		}]);