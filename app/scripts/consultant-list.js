
angular.module('mainModule')

.controller('consultantListController',
		[ '$http', '$scope', '$rootScope', '$location', 'MainService', '$window', 
		  function($http, $scope, $rootScope, $location, $ms, $window ) {

			$scope.getPhoto = function(c){
				if(c && c.photo){
					return c.photo;
				}else{
					return '../images/consultants/001.jpg';
				}
			}

			$scope.onSelectSchool = function(item){
				var query = $ms.getSessionItem('consultantFilter');
				if(query){
					if(item && item.name!=''){
						query['school'] = item.name;
					}else{
						if(query.hasOwnProperty('school'))
							delete query.school;
					}
				}else{
					if(item && item.name!=''){
						query = {school:item.name};
					}else{
						query = {};
					}
				}

				$ms.setSessionItem('consultantFilter', query);
				$ms.getConsultants(query, function(d){
					$scope.consultants = d.users;
				});
			}
			
			$scope.onSelectProgram = function(item){
				var query = $ms.getSessionItem('consultantFilter');
				if(query){
					if(item && item.name!=''){
						query['program'] = item.name;
					}else{
						if(query.hasOwnProperty('program'))
							delete query.program;
					}
				}else{
					if(item && item.name!=''){
						query = {program:item.name};
					}else{
						query = {};
					}
				}
				$ms.setSessionItem('consultantFilter', query);
				$ms.getConsultants(query, function(d){
					$scope.consultants = d.users;
				});
			}
			
			$scope.resetFilter = function(){
				var query = {};
				$scope.programs.selected = {val:'', text:''};
				$scope.schools.selected = {val:'', text:''};
				
				$ms.setSessionItem('consultantFilter', query);
				$ms.getConsultants(query, function(d){
					$scope.consultants = d.users;
				});
			}
			
			$scope.steps = [{title:'Find Advisor', img:'compass-done'},
			                {title:'Fill Application', img:'write'},
			                {title:'Find School', img:'school'},
			                {title:'Pay Fee',img:'cart'},
			                {title:'Review Paper', img:'key'},
			                {title:'Contact Professor', img:'hat'}];

			function loadFilter(){
	            var params = $location.search();
	            var query = {};
	            
	            if(params.hasOwnProperty('s') && params.s){
	            	query.school = params.s;
	            }
	            
	            if(params.hasOwnProperty('p') && params.p){
	            	query.program = params.p;
	            }
				
				$ms.getSchoolList(function(d){
	            	var school;
	            	if(query.school){
	            		school = $ms.findWhere(d, {name: query.school});
	            	}
	            	
					$scope.schools = {
							opt:{
								placeholder:'Choose School',
								valueField:'_id',
								textField: 'name'
							},
							data:d,
							selected:{text:query.school?query.school:'', val:school?school._id:''}
					}
				});
				
				$ms.getPrograms(function(d){
	            	var program;
	            	if(query.program){
	            		program = $ms.findWhere(d.programs, {name: query.program});
	            	}
					
					$scope.programs = {
							opt:{
								placeholder:'Choose Program',
								valueField:'_id',
								textField: 'name'
							},
							data:d.programs,
							selected:{text:query.program?query.program:'', val:program?program._id:''}
					}
				});
				
				return query;
			}
						
            function init(){
	            $scope.user = $ms.getSessionItem('user');
	            
	            var query = loadFilter();
				
	            $scope.consultants = [];
				$ms.getConsultants(query, function(d){
					$scope.consultants = d.users;
				});
            }
            
			$scope.redirectTo = function(a){
				$location.path(a);
			}
			
			$scope.selectConsultant = function(c){
//				if($scope.user){
					$ms.setSessionItem('consultant', c);
					$scope.redirectTo('consultant');
//				}else{
//					$scope.redirectTo('login');
//				}
			}
			
			init();
           
		}]);
