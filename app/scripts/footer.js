var RESTFUL_URL = '/api';

angular.module('mainModule')

.directive("lkFooter", function() {
  return {
    restrict: "E",       
    replace: true,
    transclude: true,      
    templateUrl: "views/footer.html",
	link: function(scope, element, attrs){
		//var h = window.screen.height;
		//$('.page-content').height(window.screen.height - $(element).height() - $('.nav-menus').height());
	}
}})

.controller('FooterController',
		[ '$http', '$scope', '$rootScope', '$location', 
		  function($http, $scope, $rootScope, $location ) {

			$scope.openTermsPage = function(){
				$location.path('legal-terms');
			}
			
			$scope.openPrivacyPage = function(){
				$location.path('privacy-policy');
			}
			
			$scope.openLoginPage = function() {
				$location.path('signin');
			}
			
			$scope.openHomePage = function() {
				$location.path('/');
			}
			
			$scope.openSignUpPage = function() {
				$location.path('signup');
			}
			
			$scope.openMyItems = function() {
				$location.path('myItems');
			}
			
			$scope.contact = function() {
				$location.path('/contacts');
			}
			
			//-------------------------------------------------
			// Events define
			//-------------------------------------------------
			$scope.$on("onUpdateFooter", function(event, args){

			});
			
			
}]);