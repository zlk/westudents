angular.module('mainModule')

.controller('AdminController',
		[ '$http', '$scope', '$rootScope', '$location', 'MainService', '$window', 
		  function($http, $scope, $rootScope, $location, $ms, $window ) {
			var bEdit = false;
			
			$scope.redirectTo = function(s){
				$location.url(s);
			}
			
			$scope.addConsultant = function(){
				bEdit = false;
				$scope.c = {username:'',password:'',email:'', role:'1', picID:'001', description:''};
			}
			
			$scope.editConsultant = function(){
				bEdit = true;
			}
			
			$scope.rmConsultant = function(){
				if(bEdit){
					$http.post(cfg.apiUrl + '/rmConsultant', {_id: $scope.c._id}).success(
							function(data, status, headers, config) {
								if (data.error && data.error.length > 0) {
	
								}else{
									$ms.getConsultants({}, function(d){
										$scope.consultants.data = d.users;
									});
									
								}
							}).error(
								function(data, status, headers, config) {
									cleanErrorMessage();
							});
				}
				bEdit = false;
			}
			
			$scope.getPhoto = function(c){
				if(c && c.photo){
					return c.photo;
				}else{
					var picID = (Math.floor(Math.random() * (12 - 1)) + 1).toString();
					var photoID =  picID.length == 1? "00" + picID : "0" + picID;
					$scope.c.photo = '../images/consultants/' + photoID + '.jpg';
					return $scope.c.photo;
				}
			}
			
			function decodeService(s){
				if(s){
					var a = s.split('|');
					$scope.createDoc.bCheck = a[0]=='y';
					$scope.modifyDoc.bCheck = a[1]=='y';
					$scope.nativeLang.bCheck = a[2]=='y';
					$scope.contactProf.bCheck = a[3]=='y';
					$scope.scholarship.bCheck = a[4]=='y';
				}else{
					$scope.createDoc.bCheck = false;
					$scope.modifyDoc.bCheck = false;
					$scope.nativeLang.bCheck = false;
					$scope.contactProf.bCheck = false;
					$scope.scholarship.bCheck = false;
				}
			}
			
			function encodeService(){
				var s = '';
				s += ($scope.createDoc.bCheck ? 'y':'n') + '|';
				s += ($scope.modifyDoc.bCheck ? 'y':'n') + '|';
				s += ($scope.nativeLang.bCheck ? 'y':'n') + '|';
				s += ($scope.contactProf.bCheck ? 'y':'n') + '|';
				s += $scope.scholarship.bCheck ? 'y':'n';
				
				return s;
			}
			
			
			$scope.onSelectRow = function(item){
				$scope.c = item;
				bEdit = true;
				decodeService($scope.c.service);
				loadPhoto(function(url){
					if(url){
						$scope.photo.src = url;
					}else{
						var picID = (Math.floor(Math.random() * (12 - 1)) + 1).toString();
						var photoID =  picID.length == 1? "00" + picID : "0" + picID;
						$scope.photo.src =  '../images/consultants/' + photoID + '.jpg';
					}
				});
				
//				setTimeout(function(){
//					$scope.$apply();
//				}, 0);
			}
			
			var Error = {
					NONE:0,
					ACCOUNT_NOT_EXIST:1,
					PASSWORD_MISMATCH:2,
					ACCOUNT_EMPTY:3,
					EMAIL_EMPTY:4,
					INVALID_EMAIL:5,
					EMAIL_EXISTS:6,
					USERNAME_EMPTY:7,
					PASSWORD_EMPTY:8,
					PASSWORD_TOO_SIMPLE:9,
					ENCRYPT_PASSWORD_EXCEPTION:10,
					UPDATE_USER_EXCEPTION:11
			};
				
			//------------------------------------------------------------------------
			// Initialize
			//------------------------------------------------------------------------

			$scope.c = {username:'',password:'',email:'', role:'1', picID:'001', description:''};
			$scope.error = {username:"", password:"", email:"", agreement:""};
			$scope.pics= ["001","002","003","004","005","006","007"];
			
			function getRandomInt(min, max) {
			    return Math.floor(Math.random() * (max - min + 1)) + min;
			}
			
			var picID = getRandomInt(1, 12).toString();
			$scope.c.picID =  picID.length == 1? "00" + picID : "0" + picID;

			$scope.getPhoto = function(photoID){
				return '../images/consultants/' + photoID + '.jpg';
			}

			
			$ms.getSchoolList(function(d){
				$scope.schools = d;
			});
			$ms.getPrograms(function(d){
				$scope.programs = d.programs;
			});
			for( var key in $scope.error){		
				$scope.$watch("error." + key, function(newVal, oldVal){
					if(newVal){
						$scope.error[key] = "";
					}
				});
			}
					
			//-------------------------------------
			// Public methods
			//-------------------------------------
			$scope.openTermsPage = function(){
				$location.path('legal-terms');
			}
			
			$scope.openPrivacyPage = function(){
				$location.path('privacy-policy');
			}
					
			$scope.signUp = function() {
				
//				cleanErrorMessage();
//				
//				if(!$scope.signup.agree){
//					$scope.error.agreement = "*In order to use our services, you must agree our Terms and privacy policy.";
//				}else{
//					$scope.error.agreement = "";
//				}
				
				var user = {username: $scope.c.username,
							password:$scope.c.password,
							email:$scope.c.email,
							photo: $scope.c.photo,
							role:2,
							description: $scope.c.description,
							program: $scope.c.program,
							school: $scope.c.school,
							service:encodeService()
							};
				
				if(bEdit){
					$http.post(cfg.apiUrl + '/updateUser', {query: {_id: $scope.c._id}, updates:{ '$set': user }}).success(
							function(data, status, headers, config) {
								if (data.error && data.error.length > 0) {
									console.log('fail: update consultant.')
								}else{
									$ms.getConsultants({}, function(d){
										$scope.consultants.data = d.users;
									});
								}
							}).error(
								function(data, status, headers, config) {
									cleanErrorMessage();
							});
				}else{
					$http.post(cfg.apiUrl + '/addUser', user).success(
							function(data, status, headers, config) {
								if (data.error && data.error.length > 0) {
									console.log('fail: add consultant.')
								}else{
									$ms.getConsultants({}, function(d){
										$scope.consultants.data = d.users;
									});
								}
							}).error(
								function(data, status, headers, config) {
									cleanErrorMessage();
							});
				}

				
			} // end of function signUp()
					
			//----------------------------------
			// Private methods
			//----------------------------------					
			function cleanErrorMessage(){
				for( var key in $scope.error){
					$scope.error[key] = "";
				}
			}
					
			function setErrorMessage(errors){
				if(errors.indexOf(Error.USERNAME_EMPTY) != -1){
					$scope.error.username = "*Please enter a username.";
				}else{
					if(errors.indexOf(Error.USERNAME_EXISTS) != -1){
						$scope.error.username = "Username exists, please use another.";
					}else{
						$scope.error.username = "";
					}
				}
				if(errors.indexOf(Error.PASSWORD_EMPTY) != -1){
					$scope.error.password = "*Please enter a password.";
				}else{
					if(errors.indexOf(Error.PASSWORD_TOO_SIMPLE) != -1){
						$scope.error.password = "At least 4 characters or numbers";//"*At least 6 characters, include uppercase, lowercase and numbers";
					}else{
						$scope.error.password = "";
					}
				}
				if(errors.indexOf(Error.EMAIL_EMPTY) != -1){
					$scope.error.email = "*Please enter your email.";
				}else{
					if(errors.indexOf(Error.INVALID_EMAIL) != -1){
						$scope.error.email = "*Email format is invalid.";
					}else{
						if(errors.indexOf(Error.EMAIL_EXISTS) != -1){
							$scope.error.email = "*Email exists, please use another.";
						}else{
							$scope.error.email = "";
						}
					}
				}
			}
			
			function getFeedback(successCb, errorCb){
				$scope.token = $ms.getSessionItem('token');
				$http.post(cfg.apiUrl + '/getFeedback', {'token': $scope.token})
					.success(function(data, status, headers, config) {
						if(successCb){
							successCb(data, status, headers, config);
						}
					})
					.error(function(){
						if(errorCb)
							errorCb();
					});
			}			
			
			function init(){
				$scope.uploader = {type:'photo', title:'Photo', 'username':''};
				$scope.photo = { src: '' };
				
				$scope.consultants = [];
				$ms.getConsultants({}, function(d){
					$scope.consultants = {
						opt:{ 
							cols: [{field:'username', w:150},
							       {field:'school', w:300},
							       {field:'program',w:200},
							       {field:'picID',w:100}],
							       
							valueField:'_id'
						},
						data: d.users
					}
					
					if(d.users)
						$scope.c = d.users[0];
					
					$scope.uploader.username = $scope.c.username;
					
				});
				
				$scope.createDoc = {bCheck:false, bEnable:true, title:'Create Document'};
				$scope.modifyDoc = {bCheck:false, bEnable:true, title:'Modify Document'};
				$scope.nativeLang = {bCheck:false, bEnable:true, title:'Native Language Review'};
				$scope.contactProf = {bCheck:false, bEnable:true, title:'Contact Professor'};
				$scope.scholarship = {bCheck:false, bEnable:true, title:'Help Apply Schoolarship'};
				
				getFeedback(function(data){
					if(data.success){
						var a = [];
						$.each(data.feedbacks, function(i,v){
							var s = v.date.split('GMT');
							v.date = s[0];
							a.push(v);
						});
						$scope.feedbacks = a;
					}else{
						alert(data.message);
					}
				});
			}
			
			// upload photo
			function loadPhoto(counsultantName, callback){
			
				$scope.uploader = {type:'photo', title:'Photo', 'username':counsultantName};		
				
				$http.post(cfg.apiUrl + '/getConsultantPhoto', {'username': counsultantName})
				.success(function (data) {
					var src = data.success ? '../' + data.photo : '';
					
					if(callback)
						callback(src);
					
				}).error(function(){
					if(callback)
						callback('');
				});
			}
			
			$scope.onCompleteUpload = function(){
				$http.post(cfg.apiUrl + '/getConsultantPhoto', {'username': $scope.c.username})
					.success(function (data) {
						$scope.photo.src = data.success ? '../' + data.photo : '';
						$scope.c.photo = $scope.photo.src;
					}).error(function(){
						$scope.photo.src = '';
					});
			}
			
			$scope.onBeforeUpload = function(data){
				return true;
			}
			
			init();
		}]);
