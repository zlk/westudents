//--------------------------------------------------------------------------------------------------
//	Author: Like Zhang 2015-03-07
//	2014-2015 Copyright reserved
//--------------------------------------------------------------------------------------------------

'use strict';
var MAX_N_PICTURES = 8;
var RESTFUL_URL = 'api';

var ItemError = {
		NONE:0,
		EMPTY:1,
		NOT_NUMBER:2,
		INVALID_CHAR:3
};

var GeoCodeStatus = {
	OK:0,
	ERROR:1
};

angular.module('mainModule').controller('ConsultantDetailController',
		[ '$scope', '$rootScope', '$location', '$http', 'MainService', function($scope, $rootScope, $location, $http, $ms){
						
			$scope.tasks = [{id:0, title:'IELTS Program', start:'1/1/2016', end:'2/1/2016', description:"Take a course to learn IELTS, and improve english", icon:"check", direction:"left"},
			                {id:1, title:'IELTS Program', start:'3/1/2016', end:'5/1/2016', description:"Take a course to learn IELTS, and improve english", icon:"credit-card", direction:"inverted"},
							{id:2, title:'IELTS Program', start:'4/1/2016', end:'9/1/2016', description:"Take a course to learn IELTS, and improve english", icon:"floppy-disk", direction:"left"},
							{id:3, title:'IELTS Program', start:'6/1/2016', end:'11/1/2016', description:"Take a course to learn IELTS, and improve english", icon:"thumbs-up", direction:"inverted"},
							{id:4, title:'IELTS Program', start:'9/1/2016', end:'12/1/2016', description:"Take a course to learn IELTS, and improve english", icon:"check", direction:"inverted"}];

			//--------------------------------------------------------------------------------
			//--------------------------------------------------------------------------------
			$scope.redirectTo = function(a){
				$location.path(a);
			}
			
			function _init(){
				$scope.consultant = $ms.getSessionItem('consultant');
				
				decodeService($scope.consultant.service);
				
				$scope.msgs = {
					    opt: {title:'Message board', 
					        from: 'student A',
		                    error: ''
					    },
					    data: [{ id: 0, dt:'', text: '' }]
					};

				$scope.msg = { 'id': 1, 'dt': '', 'text': '' };
					
				$scope.mb = { name: '', email: '', message: ''};
				$scope.error = { name: '', email: '', message: '' };

				$scope.user = $ms.getSessionItem('user');
				$scope.token = $ms.getSessionItem('token');
				
				$scope.docs = {data:[]};
				
				if($scope.user==null || $scope.token==''){
					$scope.msgs.data = [];
				}else{
					if ($scope.user.username) {
						// show messages
						$http.post(cfg.apiUrl + '/getMsg', {'token': $scope.token}).success(function (data) {
					        if(data.success)
					        	$scope.msgs.data = data.messages;
						}).error(function(){
							$scope.msgs.data = [];
						});
						
						// show documents
						$http.post(cfg.apiUrl + '/getDocs', {'token': $scope.token}).success(function (data) {
					        if(data.success){
					        	$scope.docs.data = data.docs;
					        }
						}).error(function(){
							$scope.docs.data = [];
						});
					}
				}
			}
			
			function decodeService(s){
				if(s){
					var a = s.split('|');
					$scope.services = [{bEnable:a[0]=='y', title:'Create Document', description:'Resume, Personal Statement, Reference letter from 800 to 1200 words.'},
										{bEnable:a[1]=='y', title:'Modify Document', description:'Polish the tone, choice of words, wording, grammar and logic deeply to remove the text Chinglish, but does not change the structure and content of the article.'},
										//{bEnable:a[2]=='y', title:'Native Language Review', description:'Native speaker review the document and give advice'},
										{bEnable:a[3]=='y', title:'Contact Professor', description:'Give suggestion for contact professor, Help review the email send to professor'},
										{bEnable:a[4]=='y', title:'Help Apply Schoolarship', description:'Provide options of different Schoolarship, and help review application'}];
				}else{
					$scope.services = [{bEnable:true, title:'Create Document', description:'Resume, Personal Statement, Reference letter from 800 to 1200 words.'},
										{bEnable:true, title:'Modify Document', description:'Polish the tone, choice of words, wording, grammar and logic deeply to remove the text Chinglish, but does not change the structure and content of the article.'},
										//{bEnable:true, title:'Native Language Review', },
										{bEnable:true, title:'Contact Professor', description:'Give suggestion for contact professor, Help review the email send to professor'},
										{bEnable:true, title:'Help Apply Schoolarship', description:'Provide options of different Schoolarship, and help review application'}];
				}
			}
			
			$scope.getPhoto = function(c){
				if(c && c.photo){
					return c.photo;
				}else{
					return '../images/consultants/001.jpg';
				}
			}
			
			$scope.contact = function(){
				$scope.user = $ms.getSessionItem('user');
				if ($scope.user && $scope.user.username) {
					return $location.path('message');
				}else{
					return $location.url('login');
				}
			}
			
			$scope.book = function(title){
				$location.url('booking?t=' + title);
			}
			
		    //------------------------------------------------------------------------------------
			// submit, anonymouse can submit a message
			//------------------------------------------------------------------------------------
			$scope.onSubmitMsg = function () {
			    var dt = new Date();
			    dt = $ms.getDateTimeString(dt.toString());

			    $scope.msgs.data.push({id: 1, 'dt': dt, 'text': $scope.msg.text });

			    var msg = { 'text': $scope.msg.text, 'dt': dt, 'username': $scope.user.username };

			    $http.post(cfg.apiUrl + '/postMsg', msg).success(function () {
			        console.log(cfg.apiUrl + 'postMsg' + msg);
                    $scope.msg.text = '';
				}).error(function(){
					
				});
			}// end of $scope.submitItem
			
			function validate(values){
				var rules = {
						'message':{ 
							'required' : {},
							'validChars' : {},
							'maxlength': {'params': 4096}
							}, //end of description
						'email':{
								'validChars' : {},
								'mustLeaveEmail': {
									'func': function(){
										return $scope.mb.email!='';
									},
									'message': 'You must provide email.'
								}
							}, //end of email
						'name':{
							'required' : {},
							'validChars' : {},
							'maxlength': {'params': 4096}
							}, //end of phone
						}
				
				//return ValidateService.validate(values, rules);
			}
			
			// Display error message from backend
			function errorsHandler(errors){
				var fields = Object.keys(errors);				
			}
			
			//---------------------------------------------------------------------------------------------------
			// Send http post to the server to save the item information when click the submit button
			// Submit could be add, delete, change or insert item
			//---------------------------------------------------------------------------------------------------
			function _post(item){
				$http.post(RESTFUL_URL + '/items', item).success(
						function(data, status, headers, config) {
							if(_.isEmpty(data.errorCodes)){
								// Load item to item list
								loadMyItems(item, function(_items){});
							}else{
								//errorsHandler(data.errorCodes);
								console.log("errors happend during posting item");
							}
						}).error(
						function(data, status, headers, config) {
							console.log('save item failed');
						});
			}
			
			
			$scope.$watch('item.pet_friendly', function(value) {
				if(value != null && _.has($scope, 'item')){
					if (parseInt(value) == PetFriendly.YES){
						$scope.item['pet_friendly'] = PetFriendly.YES;
					}else{
						$scope.item['pet_friendly'] = PetFriendly.NO;
					}
				}
			});
			
			
			$scope.$watch("error.name", function(newVal, oldVal){
				if(newVal){
					$scope.error.name = "";
				}
			});
			
			$scope.$watch("error.street", function(newVal, oldVal){
				if(newVal){
					$scope.error.street = "";
				}
			});
			
			$scope.$watch("error.description", function(newVal, oldVal){
				if(newVal){
					$scope.error.description = "";
				}
			});
			

			_init();
			
		} ]);
