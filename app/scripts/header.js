var RESTFUL_URL = '/api';

angular.module('mainModule')

.directive("lkHeader", ["MainService", function($ms) {
  return {
    restrict: "E",       
    replace: true,
    transclude: true,      
    templateUrl: "views/header.html",
	link: function(scope, element, attrs){
		var $menu = $(element).find('.nav-menus');
		if(!$ms.inMobile()){
			$menu.attr('data-toggle', ''); 
			$menu.attr('data-target','');
		}else{

		}
		
//		$('ul.nav li').on('click', function(){
//			$('ul.nav').hide();
//		})
	}
}}])

.controller('HeaderController', ['MainService', '$http', '$scope', '$rootScope', '$location',
		  	function($ms, $http, $scope, $rootScope, $location ) {
			
	var DEFAULT_PHOTO = '../images/consultants/001.jpg';
	
			$scope.bShowNavMenu = false;
			$scope.bMobile = false;//MobileService.isMobile();

			$scope.redirectTo = function(a){
				if($ms.inMobile()){
					$('.navbar-toggle').click();
				}
				
				if(a=='admin'){
					if($scope.isAdminLogin())
						$location.url(a);
				}else{
					$location.url(a);
				}
			}
			
			$scope.getPhoto = function(c){
				if(c && c.photo){
					return c.photo;
				}else{
					//var picID = (Math.floor(Math.random() * (12 - 1)) + 1).toString();
					//var photoID =  picID.length == 1? "00" + picID : "0" + picID;
					return '../images/consultants/' + '001' + '.jpg';
				}
			}
			
			$rootScope.$on('$routeChangeSuccess', function(e, curr, pre){
				var path = $location.path();
				if(path=='/'){
					$scope.bHomePage = true;
				}else{
					$scope.bHomePage = false;
				}
			});
			
			$scope.displayMenu = function(){
				if($scope.bMobile){
					return $scope.bShowNavMenu;
				}else{
					return true;
				}
			}
			
			$scope.showLanguageList = function(){
				$scope.bShowLanguageList = true;
			}
			
			$scope.changeLanguage = function(lang){
				$ms.changeLanguage(lang);
			}
			
			$scope.isAdminLogin = function(){
				return $scope.user && $scope.user.username == 'admin';
			}
/*			$scope.toggleMenu = function(){
				if($scope.bShowNavMenu){
					$scope.bShowNavMenu = false;
				}else{
					$scope.bShowNavMenu = true;
				}
			}*/
			//-----------------------------------------------------------------------------------------------
			// Drop down menus
			//-----------------------------------------------------------------------------------------------
			$scope.back = function(){
				history.back();
			}
			

			
			$scope.openAdminPage = function() {
				
				if(LoginService.isAdminLogin()){
					$location.path('admin');
				}
			}
			
			$scope.openLoginPage = function() {
				$location.path('signin');
			}
			
			$scope.openSignUpPage = function() {
				$location.path('signup');
			}
			
			$scope.openListing = function() {
				$location.path('propertyList');
			}
			
			$scope.newRentalPost = function() {
				//PropertyService.setFormMode(PropertyService.ItemFormMode.NEW);
				$location.path('/newRentalPost');
			}
			
			$scope.openMyListing = function() {
				//PropertyService.setFormMode(PropertyService.ItemFormMode.LISTING);
				$location.path('openMyListing');
			}
			
			$scope.contact = function() {
				$location.path('/contacts');
			}
			
			$scope.openDialog = function(){
				$rootScope.$broadcast("onOpenDialog");
			}
			

			$scope.logout = function() {
				var tokenInterval = $ms.getSessionItem('tokenInterval');
				$ms.setSessionItem('token', '');
				$ms.setSessionItem('user', '');
				var ti = $ms.getSessionItem('tokenInterval')
				clearInterval(ti);
				$ms.setSessionItem('tokenInterval','');
				//LoginService.setUser({id: '', username: '', email:'', loggedIn:false});
				updateHeader();
				$location.path('/');
				
//				$http.get(RESTFUL_URL + '/logout').success(
//						function(data, status, headers, config) {
//							var tokenInterval = $ms.getSessionItem('tokenInterval');
//							clearInterval(tokenInterval);
//							
//							LoginService.setUser({id: '', username: '', email:'', loggedIn:false});
//							updateHeader();
//							$location.path('/');
//							console.log('logout success');
//						}).error(
//						function(data, status, headers, config) {
//							LoginService.setUser({id: '', username: '', email:'', loggedIn:false});
//							updateHeader();
//							$location.path('/');
//							console.log('logout failed');
//						});
			}
			
			function updateHeader(){
				$scope.user = $ms.getSessionItem('user');
				if($scope.user){
					$scope.loggedIn = true;
				}else{
					$scope.loggedIn = false;
				}
				setTimeout(function(){
					$scope.$apply();
				}, 0);
			}
			
			//-------------------------------------------------
			// Events define
			//-------------------------------------------------
			$scope.$on("onUpdateHeader", function(event, args){
				$scope.user = $ms.getSessionItem('user');
				updateHeader();
			});
			
			$scope.$on("onLogout", function(event,args){
				$scope.logout();
			});
			
			$scope.$watch('bShowNavMenu',function(){
				
			});

			function _init(){
				$scope.user = $ms.getSessionItem('user');
				
				if(!$scope.user){
					$scope.user = {username:'Anonymous', photo:DEFAULT_PHOTO};
				}
				
				// update header according to the current login user or anonymous
				$ms.securePost('/getAccount', {},
						function(data, status, headers, config) {
							// Force to logout within an hour
							//setTimeout($scope.logout, 60*60*1000);
							$scope.loggedIn = data.success;
							setTimeout(function(){
								$scope.$apply();
							}, 0);
						},
						function(data, status, headers, config) {
							$scope.loggedIn = false;
							setTimeout(function(){
								$scope.$apply();
							}, 0);
						});
			}
			
			_init();
}])
/*
.animation('.slide', function() {
    var NG_HIDE_CLASS = 'ng-hide';
    return {
        beforeAddClass: function(element, className, done) {
            if(className === NG_HIDE_CLASS) {
                element.slideUp(done); 
            }
        },
        removeClass: function(element, className, done) {
            if(className === NG_HIDE_CLASS) {
                element.hide().slideDown(done);
            }
        }
    }
});
*/
