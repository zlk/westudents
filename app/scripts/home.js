'use strict';

//---------------------------------------------------------------------------------------------------------------
// Home controller
//---------------------------------------------------------------------------------------------------------------
angular.module('mainModule')

.controller('HomeController', [ '$scope', '$rootScope', '$http', '$location', 'MainService',
		  function( $scope, $rootScope, $http, $location, $ms) {
	
		
		$scope.types = [{id:0, title:'Research Master'},
		               {id:1, title:'Taught Master'}];

		$scope.getPhoto = function(photoID){
			return '../images/consultants/' + photoID + '.jpg';
		}
		
		
		$scope.achievments = [ {id:0, username:'Marc Zhang', img:'/images/consultants/011.jpg', minutes:50, 
			school:'Toronto University', enroll:'May 2016', 
			task:'Chemical Exam', status:"done", message:'I got 89 today, Congratulate myself!!!', nComments: 2},
			
			{id:0, username:'Lisa Wang', img:'/images/consultants/010.jpg' ,minutes: 20, 
				school:'Waterloo University', enroll: 'Jan 2017',
				task:'Personal Statement', status:"done", message:'lol, spend me a whole week, finally done. What is yours?', nComments: 1},
			]
		
		$scope.item = {planYear:'', major:'', type: '', currEnroll:''};
		
		$scope.openSchoolSelect = function(){
			$rootScope.$broadcast('OnShowSchoolSelect');
		}
		
		$scope.getImage = function(imageID){
			return '../images/schools/' + imageID + '.png';
		}
		
		$scope.selectService = function(s){
			if(s.title == 'Review Paper')
				$location.path('document');
			else if(s.title == 'My Plan')
				$location.path('plan');
			else if(s.title == 'Contact Professor')
				$location.path('professor');
		}
		
		$scope.showSchedule = function(){
			$scope.bSchedule = true;
			//$scope.bForm = false;
		}
		
		$scope.savePlan = function(){
			$rootScope.$broadcast('OnShowSignup');
		}
		
		$ms.resize();
	
		$scope.bMobile = false;//MobileService.isMobile();
		$scope.showHome = true;
		$scope.showPropertyDetail = false;
	
		$scope.placeholderVal = "School";
		
		$scope.bSearchIcon = true;
		
		var SearchType = 0; //SearchService.SearchType;
		var ItemStatus = 0; //PropertyService.ItemStatus;
		
		$scope.signup = {username:'',password:'',email:'', role:'1', usernameMsg :"", passwordMsg:"", emailMsg:"", agreementMsg:""};
		
		$scope.signupTitle = "Sign up to post free Ads today";
		//$scope.seachboxTitle = "Find a rental housing in Canada";
		
		//$scope.seachboxTitle = gettextCatalog.getString("Find a rental housing in Canada");

			//----------------------------------------------------------------------------------
			// Public methods
			//----------------------------------------------------------------------------------
			$scope.redirectTo = function(a){
				$location.path(a);
			}
			
			$scope.selectConsultant = function(c){
				if($scope.user){
					$scope.redirectTo('dashboard');
				}else{
					$scope.redirectTo('signup');
				}
			}
			
			$scope.openSignUpPage = function() {
				$location.path('signup');
			}
			
			$scope.search = function(){
				var s = '', p = '';
				if($scope.schools.selected){
					s = $scope.schools.selected.text;
				}
				if($scope.programs.selected){
					p = $scope.programs.selected.text;
				}
				
				$location.url('consultants?s='+ s +'&p=' + p);
			}
			
			
			function _saveTraffic(callback){
				$http.post(cfg.apiUrl + '/saveTraffic')
					.success(function(data, status, headers, config) {
						if(callback){
							callback(data);
						}
					})
					.error(function(data, status, headers, config) {
						if(callback){
							callback(data);
						}
						console.log('[failed] --- save traffic');
					});
			}
			
			function _countTraffic(callback){
				$http.post(RESTFUL_URL + '/getTrafficCount')
					.success(function(data, status, headers, config) {
						if(callback){
							callback(data);
						}
					})
					.error(function(data, status, headers, config) {
						if(callback){
							callback(data);
						}
						console.log('[failed] --- count traffic');
					});
			}
			

			
			function _initSearchBox(){
	            $ms.getSchoolList(function(d){					
					$scope.schools = {
							opt:{
								placeholder:'Which school you want to apply ?',
								valueField:'_id',
								textField: 'name'
							},
							data:d,
							selected:{text:'', val:''}
					}
				});
	            
				$ms.getPrograms(function(d){					
					$scope.programs = {
							opt:{
								placeholder:'Which program ?',
								valueField:'_id',
								textField: 'name'
							},
							data:d.programs,
							selected:{text:'', val:''}
					}
				});
			}
			
			function initTokenTask(){
				var t = $ms.getSessionItem('token');
				
				if(t){
					var tokenInterval = setInterval(function(){
						var token = $ms.getSessionItem('token');
						$ms.renewToken(token, function(newToken, errMsg){
							if(newToken){
								$ms.setSessionItem('token', newToken);
							}else{
								var tokenInterval = $ms.getSessionItem('tokenInterval');
								clearInterval(tokenInterval);
								
								//if(loggedIn)
									//alert(errMsg);
							}
						})
					}, 4*60000);
				
					$ms.setSessionItem('tokenInterval', tokenInterval);
				}
			}
			
			//--------------------------------------------------------------------
			// When navigate to the home page, init() will be called to 
			// 1. load filter from the cookie
			// 2. send http request to get the item list and save
			//--------------------------------------------------------------------
			function init(){
	            $scope.user = $ms.getSessionItem('user');
	            
	            _initSearchBox();
	            
	            // To handle refresh
	            initTokenTask();
	            
				_saveTraffic(function(){
					_countTraffic(function(c){
						if(c){
							$scope.userCount = c.count;
						}
					})
				});
//				updateCityList();
			}
			
			init();
			
		} ])
