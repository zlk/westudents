angular.module('mainModule')

.controller('ClientController',
		[ '$http', '$scope', '$rootScope', '$location', 'MainService', '$window', 
		  function($http, $scope, $rootScope, $location, $ms, $window ) {


			
			$scope.redirectTo = function(s){
				$location.url(s);
			}
			
			$scope.getPhoto = function(c){
				if(c && c.photo){
					return c.photo;
				}else{
					return '../images/consultants/001.jpg';
				}
			}
			
			$scope.onSelectClient = function(item){
				$scope.client = item.client;
				$http.post(cfg.apiUrl + '/getUserPhoto', {'username': item.client})
					.success(function (data) {
						$scope.msgs = {
							    opt: { 
							    	'title':'Message board', 
							    	'to': {username: item.client, photo: data.success ? '../' + data.photo : ''}, 
							    	'from': {username: item.consultant, photo:$scope.getPhoto($scope.consultant)},
				                    'error': ''
							    },
							    data: []
							};
					}).error(function(){
						$scope.msgs = {
							    opt: { 
							    	'title':'Message board', 
							    	'to': {username: item.client, photo: ''}, 
							    	'from': {username: item.consultant, photo:$scope.getPhoto($scope.consultant)},
				                    'error': ''
							    },
							    data: []
							};
					});

				
				
				getMessage(item.client, function(d){
					$scope.msgs.data = d.messages;
				})
			}
			
			function getContact(successCb, failCb){
			    var query = {'consultant': $scope.consultant.username };

			    $http.post(cfg.apiUrl + '/getContact', {'token': $scope.token, 'query': query}).success(function (data) {
			    	if(data.success){
	                    if(successCb)
	                    	successCb(data);
			    	}else{
						if(failCb)
							failCb();
			    	}
				}).error(function(){
					if(failCb)
						failCb();
				});
			}
			
			function getMessage(username, successCb, failCb){
			    var query = { $or: [{'from': username}, {'from': $scope.consultant.username }] };

			    $http.post(cfg.apiUrl + '/getMsg', {'token': $scope.token, 'query': query}).success(function (data) {
			    	if(data.success){
	                    if(successCb)
	                    	successCb(data);
			    	}else{
						if(failCb)
							failCb();
			    	}
				}).error(function(){
					if(failCb)
						failCb();
				});
			}
			

			function postMessage(successCb, failCb){
			    var dt = new Date();
			    dt = $ms.getDateTimeString(dt.toString());
			    
			    if($scope.client){
			    
			    var msg = { 'text': $scope.msg.text, 'dt': dt, 'from': $scope.consultant.username, 'to': $scope.client };

				    $http.post(cfg.apiUrl + '/postMsg', msg).success(function () {
	                    $scope.msg.text = '';
	                    if(successCb)
	                    	successCb();
					}).error(function(){
						if(failCb)
							failCb();
					});
			    }
			}

			$scope.onSubmitMsg = function () {
			    //$scope.msgs.data.push({id: 1, 'dt': dt, 'text': $scope.msg.text });
				postMessage(function(){
					getMessage($scope.client, function(data){
						$scope.msgs.data = data.messages;
					});
				});
			}// end of $scope.submitItem
			
			var Error = {
					NONE:0,
					ACCOUNT_NOT_EXIST:1,
					PASSWORD_MISMATCH:2,
					ACCOUNT_EMPTY:3,
					EMAIL_EMPTY:4,
					INVALID_EMAIL:5,
					EMAIL_EXISTS:6,
					USERNAME_EMPTY:7,
					PASSWORD_EMPTY:8,
					PASSWORD_TOO_SIMPLE:9,
					ENCRYPT_PASSWORD_EXCEPTION:10,
					UPDATE_USER_EXCEPTION:11
			};
				
			
			
			//------------------------------------------------------------------------
			// Initialize
			//------------------------------------------------------------------------


			function init(){
				$scope.consultant = $ms.getSessionItem('consultant');
				$scope.token = $ms.getSessionItem('token');
				
				$scope.msgs = {
					    opt: { 
					    	'title':'Message board', 
					    	//'to': {username: item.client, photo: data.success ? '../' + data.photo : ''}, 
					    	'from': {username: $scope.consultant.username, photo:$scope.getPhoto($scope.consultant)},
		                    'error': ''
					    },
					    data: []
					};
				
				getContact(function(data){
					$scope.clients = {
							opt:{ 
								cols: [{field:'client', w:100},
								       {field:'dt', w:220}],
								valueField:'_id'
							},
							data: data.contacts
						}
				});
			}
			
			init();
		}]);
