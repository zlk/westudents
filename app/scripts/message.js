//--------------------------------------------------------------------------------------------------
//	Author: Like Zhang 2015-03-07
//	2014-2015 Copyright reserved
//--------------------------------------------------------------------------------------------------

'use strict';
var MAX_N_PICTURES = 8;
var RESTFUL_URL = 'api';

var ItemError = {
		NONE:0,
		EMPTY:1,
		NOT_NUMBER:2,
		INVALID_CHAR:3
};

var GeoCodeStatus = {
	OK:0,
	ERROR:1
};

angular.module('mainModule').controller('messageController',
		[ '$scope', '$rootScope', '$location', '$http', 'MainService','formatFilter', function($scope, $rootScope, $location, $http, $ms, formatFilter){
						
			$scope.tasks = [{id:0, title:'IELTS Program', start:'1/1/2016', end:'2/1/2016', description:"Take a course to learn IELTS, and improve english", icon:"check", direction:"left"},
			                {id:1, title:'IELTS Program', start:'3/1/2016', end:'5/1/2016', description:"Take a course to learn IELTS, and improve english", icon:"credit-card", direction:"inverted"},
							{id:2, title:'IELTS Program', start:'4/1/2016', end:'9/1/2016', description:"Take a course to learn IELTS, and improve english", icon:"floppy-disk", direction:"left"},
							{id:3, title:'IELTS Program', start:'6/1/2016', end:'11/1/2016', description:"Take a course to learn IELTS, and improve english", icon:"thumbs-up", direction:"inverted"},
							{id:4, title:'IELTS Program', start:'9/1/2016', end:'12/1/2016', description:"Take a course to learn IELTS, and improve english", icon:"check", direction:"inverted"}];

			//--------------------------------------------------------------------------------
			//--------------------------------------------------------------------------------
			function getContacts(query, successCb, failCb){
				
				$scope.token = $ms.getSessionItem('token');
			
			    $http.post(cfg.apiUrl + '/getContact', {'token': $scope.token, 'query':query}).success(function (data) {
			    	if(data.success){
	                    if(successCb)
	                    	successCb(data);
			    	}else{
						if(failCb)
							failCb();
			    	}
				}).error(function(){
					if(failCb)
						failCb();
				});
			}
			
			$scope.getPhoto = function(c){
				if(c && c.photo){
					return c.photo;
				}else{
					return '../images/consultants/001.jpg';
				}
			}
			
			function _loadData(){
				$scope.msgs = {
					    opt: { 
					    	'title':'Message board', 
					    	'from': {'username': $scope.user.username, 'photo': $scope.getPhoto($scope.user)}, 
					    	'to': {'username': $scope.consultant.username, 'photo':$scope.getPhoto($scope.consultant)},
		                    'error': ''
					    },
					    data: []
					};

				$scope.msg = { 'id': 1, 'dt': '', 'text': '' };
					
				$scope.mb = { name: '', email: '', message: ''};
				$scope.error = { name: '', email: '', message: '' };

				
				$scope.token = $ms.getSessionItem('token');
				
				$scope.docs = {data:[]};
				
				if($scope.user==null || $scope.token==''){
					$scope.msgs.data = [];
				}else{
					if ($scope.user.username) {
						// show messages
						getMessage(function (data) {
					        $scope.msgs.data = data.messages;
						}, function(){
							$scope.msgs.data = [];
						});
						
						// show documents
						$http.post(cfg.apiUrl + '/getDocs', {'token': $scope.token}).success(function (data) {
					        if(data.success){
					        	$scope.docs.data = data.docs;
					        }
						}).error(function(){
							$scope.docs.data = [];
						});
					}
				}
			}
			
			function isConsultant(){
				return $scope.user && $scope.user.role == '2';
			}
			
			function _init(){
				
				$scope.user = $ms.getSessionItem('user');
				if(isConsultant()){
					// load clients that have conversation
					getContacts({ 'consultant': $scope.user.username }, function(d){
						if(d.success && d.contacts.length > 0){
							$scope.conctacts = d.contacts;
						}else{
							$scope.contacts = [];
						}
					});
				}else{
					// load consultants that have conversation
					getContacts({ 'client': $scope.user.username }, function(d){
						if(d.success && d.contacts.length > 0){
							$scope.contacts = d.contacts;
						}else{
							$scope.contacts = [];							
						}
					});
				}
				
				
				$scope.consultant = $ms.getSessionItem('consultant');
				if($scope.consultant){
					$scope.hasConsultant = true;
					_loadData();
				}else{
					getContacts({ 'client': $scope.user.username }, function(d){
						if(d.success && d.contacts.length > 0){
							var c = d.contacts[0];
							
							$http.post(cfg.apiUrl + '/getConsultantDetail', {'username': c.consultant})
							.success(function (data) {
						        if(data.success){
						        	$scope.consultant = data.consultant;
						        	$scope.hasConsultant = true;
						        	_loadData();
						        }else{
						        	$scope.hasConsultant = $scope.consultant? true: false;
						        }
							}).error(function(){
								$scope.hasConsultant = $scope.consultant? true: false;
							});
						}else{
							$scope.hasConsultant = $scope.consultant? true: false;
						}
					});
				}
			}
			

			
			$scope.redirectTo = function(a){
				$location.path(a);
			}
			
			function postMessage(successCb, failCb){
			    var dt = new Date();
			    dt = $ms.getDateTimeString(dt.toString());
			    var msg = { 'text': $scope.msg.text, 'dt': dt, 'from': $scope.user.username, 'to': $scope.consultant.username };

			    $http.post(cfg.apiUrl + '/postMsg', msg).success(function () {
                    $scope.msg.text = '';
                    if(successCb)
                    	successCb();
				}).error(function(){
					if(failCb)
						failCb();
				});
			}

			function getMessage(successCb, failCb){
			    var query = { $or: [{'from': $scope.user.username}, {'from': $scope.consultant.username }] };

			    $http.post(cfg.apiUrl + '/getMsg', {'token': $scope.token, 'query': query}).success(function (data) {
			    	if(data.success){
	                    if(successCb)
	                    	successCb(data);
			    	}else{
						if(failCb)
							failCb();
			    	}
				}).error(function(){
					if(failCb)
						failCb();
				});
			}
			
			function addContact(successCb, failCb){
			    var dt = new Date();
			    dt = $ms.getDateTimeString(dt.toString());
				var c = { 'client': $scope.user.username, 'consultant': $scope.consultant.username, 'dt':dt };
			    $http.post(cfg.apiUrl + '/addContact', { 'token': $scope.token, 'contact':c }).success(function (data) {
			    	if(data.success){
	                    if(successCb)
	                    	successCb(data);
			    	}else{
						if(failCb)
							failCb();
			    	}
				}).error(function(){
					if(failCb)
						failCb();
				});
			}
			
			
		    //------------------------------------------------------------------------------------
			// submit, anonymouse can submit a message
			//------------------------------------------------------------------------------------
			$scope.onSubmitMsg = function () {
			    //$scope.msgs.data.push({id: 1, 'dt': dt, 'text': $scope.msg.text });
				postMessage(function(){
					
					addContact();
					
					getMessage(function(data){
						$scope.msgs.data = data.messages;
					});
					
				});
			}// end of $scope.submitItem
			
			function validate(values){
				var rules = {
						'message':{ 
							'required' : {},
							'validChars' : {},
							'maxlength': {'params': 4096}
							}, //end of description
						'email':{
								'validChars' : {},
								'mustLeaveEmail': {
									'func': function(){
										return $scope.mb.email!='';
									},
									'message': 'You must provide email.'
								}
							}, //end of email
						'name':{
							'required' : {},
							'validChars' : {},
							'maxlength': {'params': 4096}
							}, //end of phone
						}
				
				//return ValidateService.validate(values, rules);
			}
			
			// Display error message from backend
			function errorsHandler(errors){
				var fields = Object.keys(errors);				
			}
			
			//---------------------------------------------------------------------------------------------------
			// Send http post to the server to save the item information when click the submit button
			// Submit could be add, delete, change or insert item
			//---------------------------------------------------------------------------------------------------
			function _post(item){
				$http.post(RESTFUL_URL + '/items', item).success(
						function(data, status, headers, config) {
							if(_.isEmpty(data.errorCodes)){
								// Load item to item list
								loadMyItems(item, function(_items){});
							}else{
								//errorsHandler(data.errorCodes);
								console.log("errors happend during posting item");
							}
						}).error(
						function(data, status, headers, config) {
							console.log('save item failed');
						});
			}
			
			
			
			$scope.$watch('item.pet_friendly', function(value) {
				if(value != null && _.has($scope, 'item')){
					if (parseInt(value) == PetFriendly.YES){
						$scope.item['pet_friendly'] = PetFriendly.YES;
					}else{
						$scope.item['pet_friendly'] = PetFriendly.NO;
					}
				}
			});
			

			
			$scope.$watch("error.name", function(newVal, oldVal){
				if(newVal){
					$scope.error.name = "";
				}
			});
			
			$scope.$watch("error.street", function(newVal, oldVal){
				if(newVal){
					$scope.error.street = "";
				}
			});
			
			$scope.$watch("error.description", function(newVal, oldVal){
				if(newVal){
					$scope.error.description = "";
				}
			});
			

			_init();
			
		} ]);
