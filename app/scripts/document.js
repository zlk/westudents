
angular.module('mainModule')

.controller('documentController',
		[ '$http', '$scope', '$rootScope', '$location', 'MainService', '$window', 
		  function($http, $scope, $rootScope, $location, $ms, $window ) {


			$ms.getConsultants({}, function(d){
				$scope.consultants = d.users;
			});
			
			$scope.onSelectSchool = function(text){
				var query = $ms.getSessionItem('consultantFilter');
				if(query){
					if(text!=''){
						query['school'] = text;
					}else{
						if(query.hasOwnProperty('school'))
							delete query.school;
					}
				}else{
					if(text!=''){
						query = {school:text};
					}else{
						query = {};
					}
				}

				$ms.setSessionItem('consultantFilter', query);
				
				$ms.getConsultants(query, function(d){
					$scope.consultants = d.users;
				});
			}
			
			$scope.onSelectProgram = function(text){
				var query = $ms.getSessionItem('consultantFilter');
				if(query){
					if(text!=''){
						query['program'] = text;
					}else{
						if(query.hasOwnProperty('program'))
							delete query.program;
					}
				}else{
					if(text!=''){
						query = {program:text};
					}else{
						query = {};
					}
				}

				$ms.setSessionItem('consultantFilter', query);
				$ms.getConsultants(query, function(d){
					$scope.consultants = d.users;
				});
			}
			
			$scope.resetFilter = function(){
				var query = {};
				$scope.programs.selected = '';
				$scope.schools.selected = '';
				
				$ms.setSessionItem('consultantFilter', query);
				$ms.getConsultants(query, function(d){
					$scope.consultants = d.users;
				});
				
				$scope.selectedConsultant = null;
			}
			
			$scope.selectConsultant = function(c){
				if($scope.selectedConsultant){
//					var found = $scope.consultants.filter(function(el){
//						return el._id == $scope.selectedConsultant._id;
//					});
					
//					if(!found){
						$scope.consultants.push($scope.selectedConsultant);
//					}
				}
				
				$scope.consultants = $scope.consultants.filter(function(el){
					return el._id != c._id;
				});
				
				$scope.selectedConsultant = c;
			}
			
			$scope.steps = [{title:'Find Advisor', img:'compass-done'},
			                {title:'Fill Application', img:'write'},
			                {title:'Find School', img:'school'},
			                {title:'Pay Fee',img:'cart'},
			                {title:'Review Paper', img:'key'},
			                {title:'Contact Professor', img:'hat'}];
			
			$scope.getPhoto = function(photoID){
				if(photoID)
					return '../images/consultants/' + photoID + '.jpg';
				else
					return '';
			}
			
			
			var layout = {page:{w:800}};
			
            $scope.multiPages = {
                    opt: {
                        w: layout.page.w,
                        steps: [{ id: 1, title: "Choose Consultant", validate: null },
                                { id: 2, title: "Upload Document", validate: null },
                                { id: 3, title: "Pay Fee" }
                        ]
                    },
                    selected: { index: 0 }
                };
            
            var params = $location.search();
            
            if(params.hasOwnProperty('PayerID')){
            	$scope.multiPages.selected = {index:2};
            	$scope.bConfirm = true;
            }else{
            	$scope.bConfirm = false;
            }
            
            
            $scope.user = $ms.getSessionItem('user');
            
            var username = '';
            if($scope.user){
            	username = $scope.user.username;
            }else{
            	$location.path('login');
            }
            
        	$scope.types = [{type:'Resume', title:'Resume', 'username':username},
        	                {type:'Personal Statement', title:'Personal Statement', 'username':username},
        	                {type:'Reference Letter', title:'Reference Letter', 'username':username}];
            
			$ms.getSchoolList(function(d){
				var schools = [];
				$.each(d, function(i, v){
					schools.push(v.name);
				});
				
				$scope.schools = {
						opt:{
							placeholder:'Choose School',
						},
						data:schools,
						selected:''
				}
			});
			
			$ms.getPrograms(function(d){
				var programs = [];
				$.each(d.programs, function(i, v){
					programs.push(v.name);
				});
				
				$scope.programs = {
						opt:{
							placeholder:'Choose Major',
						},
						data:programs,
						selected:''
				}
			});
            
			// show documents
			$scope.token = $ms.getSessionItem('token');
			$scope.docs = {data:[]};
			
			
			function loadDocs(){
				$http.post(cfg.apiUrl + '/getDocs', {'token': $scope.token}).success(function (data) {
			        if(data.success){
			        	$scope.docs.data = data.docs;
			        	updateCart();
			        }
				}).error(function(){
					$scope.docs.data = [];
				});	
			}
			
			loadDocs();
			
			function findWhere(a, obj) {
				if (a) {
					for (var i = 0; i < a.length; i++) {
						var v = a[i];
						var matched = true;

						for (var key in obj) {
							if (!(key in v && v[key] == obj[key])) {
								matched = false;
								break;
							}
						}

						if (matched) {
							return v;
						}
					}
				}
				return null;
			}
			
			$scope.rmDoc = function(d){
				var opt = $.extend({'token': $scope.token}, d);
				$http.post(cfg.apiUrl + '/rmDoc', opt).success(function (data) {
					//if(data.success){
						//$scope.docs.data = data.docs;
						$scope.docs.data = $scope.docs.data.filter(function(el){
							return el.fname != d.fname || el.created != d.created;
						});
						
						updateCart();
					//}
				}).error(function(){
					$scope.docs.data = [];
				});
			}
			
            $scope.goPaypal = function(){
            	var total = $scope.getTotal();
            	
            	$ms.setSessionItem('total', total);
            	
            	$http.post(cfg.apiUrl + '/setCheckout', {amount: total})
            		.success(function(data, status) {
            			// When redirecting the buyer back to your website from paypal.com, PayPal appends the Express Checkout transaction token 
            			// and the unique PayPal buyer ID as GET parameters to your RETURN URL; these GET parameters are named token and PayerID.
							$window.location.href = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" + data.TOKEN;
						})
					.error(function(data, status, headers, config) {
							
						});
            	
            	//$scope.getExpressCheckout(function(data){
            	//	$location.url = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" + data.TOKEN;
            	//})
            }
            
            $scope.products = [{name:'Resume', price:120, currency:'CAD'},
                               {name:'Personal Statement', price:120, currency:'CAD'},
								{name:'Reference Letter', price:120, currency:'CAD'}];
            
            
			
            $scope.cart = [];
			function updateCart(){
				$scope.cart = [];
				$.each($scope.docs.data, function(i, doc){
					var p = findWhere($scope.products, {name: doc.type});
					if(p){
						doc.price = p.price;
						doc.currency = p.currency;
					}
					$scope.cart.push(doc);
				})
			}
			
			
			$scope.getTotal = function(){
				var total = 0;
				$.each($scope.cart, function(i, v){
					total += v.price;
				})
				return total;
			}
			
			function findWhere(a, obj) {
				if (a) {
					for (var i = 0; i < a.length; i++) {
						var v = a[i];
						var matched = true;

						for (var key in obj) {
							if (!(key in v && v[key] == obj[key])) {
								matched = false;
								break;
							}
						}

						if (matched) {
							return v;
						}
					}
				}

				return null;
			}
			
			// confirm checkout
			$scope.confirm = function(){
	            var params = $location.search();
	            if(params.hasOwnProperty('PayerID')){
	            	var total = $ms.getSessionItem('total');
	            	var payerID = params.PayerID;
	            	var token = params.token;
	            	
	            	$http.post(cfg.apiUrl + '/checkout', {'amount': total,
	            		'token': token,
	            		'payerID': payerID})
            		.success(function(data, status) {
            			$ms.showSplashDialog({body:'Payment successful!'});
            		})
					.error(function(data, status, headers, config) {
							
					});
	            }
			}
			
			$scope.onCompleteUpload = function(){
				loadDocs();
			}
			
			$scope.onBeforeUpload = function(item){
				if(!($scope.user && $scope.user.username)){
					$location.path('login');
					return false;
				}else{
					return true;
				}
			}
		}]);