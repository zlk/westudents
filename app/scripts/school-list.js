
angular.module('mainModule')

.controller('schoolListController',
		[ '$http', '$scope', '$rootScope', '$location', function($http, $scope, $rootScope, $location ) {

			$scope.consultants = [ {name: 'Vicky', gender:'F', photoID: '001', school: 'Toronto University', schoolType: 'University', program: 'Business Accounting', degree: 'Master', current: 'Studing', nSuccessCases: 5 },
			                       {name: 'Henry', gender:'M', photoID: '010', school: 'Toronto University', schoolType: 'University', program: 'Business Accounting', degree: 'Master', current: 'Studing', nSuccessCases: 2 },
			                       {name: 'Kevin', gender:'M', photoID: '011', school: 'Conestoga College', schoolType: 'College', program: 'Computer Science', degree: 'Bachelor', current: 'Studing', nSuccessCases: 3 },
			                       {name: 'Jenias', gender:'F', photoID: '002', school: 'Guelph University', schoolType: 'University', program: 'Plant Science', degree: 'Master', current: 'Studing', nSuccessCases: 2 },
			                       {name: 'Vivian', gender:'F', photoID: '003', school: 'Guelph University', schoolType: 'University', program: 'Animal Science', degree: 'Master', current: 'Studing', nSuccessCases: 2 }
			                      ];
			
			$scope.schools = [{id: 0, name:'Conestoga College', type: 'College', imageID:'conestoga'},
			                  {id: 1, name:'Fanshawe College', type: 'College', imageID:'fanshawe'},
			                  {id: 2, name:'Guelph University', type: 'University',imageID:'guelph'},
			                  {id: 3, name:'Toronto University', type: 'University',imageID:'toronto'},
			                  {id: 4, name:'Waterloo University', type: 'University',imageID:'waterloo'},
			                  {id: 5, name:'Western Ontario University', type: 'University',imageID:'western'}
			                  ];
			
			$scope.programs = [{id: 0, name:'Animal Science', school: 'Guelph University'},
			                    {id: 1, name:'Plant Science', school: 'Guelph University'},
			                    {id: 2, name:'Computer Science', school: 'Conestoga College'},
			                    {id: 3, name: 'Business Accounting', school:'Toronto University'}];
			
			$scope.getImage = function(imageID){
				return '../images/schools/' + imageID + '.png';
			}
			
			$scope.toggleSchoolType = function(t){
				
			}
		}]);