'use strict';
                      
/* create root module westudentsApp */
angular.module('westudentsApp', ['ngResource', 'ngRoute', 'ngCookies', 'ngSanitize', 'ngTouch', 'ngAnimate',
								 'pascalprecht.translate', 'rzModule',
                                 'mainModule','auth','wsui'
	                             //'ui.bootstrap'
                           ])
//-------------------------------------------------------------------------------
// Get executed during the provider registrations and configuration phase. 
// Only providers and constants can be injected into configuration blocks.
//-------------------------------------------------------------------------------
.config(['$routeProvider', '$translateProvider',  function($routeProvider, $translateProvider){
//.config(['$routeProvider',  function($routeProvider){	
	// function getOrientation(){
	    // return Math.abs(window.orientation) - 90 == 0 ? "landscape" : "portrait";
	// };

	// var bMobile = (function(){
		// //if(window.screen.width <= 800 && window.height <= 800){
		// if(window.innerWidth <= 640 ){
			// return true;
		// }else{
			// return false;
		// }
	// })();
	
    $routeProvider
	    .when('/', {
	      templateUrl: '/views/home.html',
	      controller: 'HomeController'
	    })
	   .when('/admin',{
			 templateUrl: '/views/admin.html'	
	   })
	   .when('/client',{
			 templateUrl: '/views/client.html'	
	   })
	   .when('/signupConsultant',{
		 templateUrl: '/views/signup-consultant.html'
	   })
	   .when('/signup',{
		 templateUrl: '/views/signup.html'
	   })
	   .when('/login',{
			 templateUrl: '/views/login.html'	
	   })
	   .when('/profile',{
			 templateUrl: '/views/user-profile.html'	
	   })
	  // .when('/forgetPassword',{
			// templateUrl: '/views/forget-password.html',
			// controller: 'ForgetPasswordController'		
	  // })
	  // .when('/resetPassword',{
			// templateUrl: '/views/reset-password.html',
			// controller: 'ResetPasswordController'		
	  // })
	  // .when('/passwordStatus',{
			// templateUrl: '/views/reset-password-status.html',
			// controller: 'PasswordStatusController'		
	  // })
	   .when('/schools',{
	  		templateUrl: '/views/school-list.html'
  	   })
 	   .when('/consultants',{
	  		templateUrl: '/views/consultant-list.html'
	   })
 	   .when('/consultant',{
	  		templateUrl: '/views/consultant-detail.html'
	   })
	   .when('/document',{
	  		templateUrl: '/views/document.html'
	   })
	   .when('/professor',{
	  		templateUrl: '/views/professor.html'
	   })
  	   .when('/application-form', {
			templateUrl: '/views/application-form.html'
  	  	})
  	   .when('/dashboard',{
	   		templateUrl: '/views/dashboard.html'
	   })	  	  
	   .when('/plan', {
			 templateUrl: '/views/plan.html'
	   })
	   .when('/contact',{
			 templateUrl:'/views/contact.html'
       })
       .when('/booking',{
			 templateUrl: '/views/booking.html'	
	   })
       .when('/message',{
			 templateUrl: '/views/message.html'	
	   })
	   // .when('/legal-terms',{
			// templateUrl:'/views/terms.html',
			// controller: 'termsController'
      // })
      // .when('/privacy-policy',{
			// templateUrl:'/views/privacy.html',
			// controller: 'privacyController'
      // })
      .otherwise({
    	  	redirectTo: '/'
       });
    
    // Translation
    $translateProvider.translations('en_US', en_US );  
    $translateProvider.translations('zh_CN', zh_CN );
    
    $translateProvider.preferredLanguage('zh_CN');
}]);

