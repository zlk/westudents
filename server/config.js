//------------------------------------------------------
// Author: zlk
// Date: Oct 8 2015
// License: zlk license
// All right reserved
//------------------------------------------------------

'use strict'


var mode = "developement";

var rootPath = '/home/dustin/apps/westudents/';

if(mode=="production"){
	rootPath = '/home/dustin/apps/westudents/';
}else{
	rootPath = 'c:/workspace/westudents';
}

module.exports = {
	port:5003,
	dbHost: 'localhost',
	dbName: 'westudents',
	dbPort: '27017',
	sessionPrefix: 'westudents',
	imageRootFolder: rootPath + '/app/',
	storageRoot:rootPath + '/app/',
	logFile: rootPath + '/log.txt',
	sendgrid:{
		username:'zlk',
		password:'052980!ucK'
	},
	jwt:{
		secret: 'zlkhmacsha256',
		algorithm: 'HS256',
		expiresInSeconds: 300
	}
}
