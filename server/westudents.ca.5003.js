var http = require('http');
var Config = require('./config');
var express = require("express"),
	app = express(),
	port = parseInt(process.env.PORT,10) || Config.port;

// body-parser does not handle multipart bodies
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');

//parse application/x-www-form-urlencoded
//app.use(bodyParser.urlencoded({ extended: false, limit:1024 * 1024 * 1000 }))
app.use(bodyParser.urlencoded({ extended: false, limit: '1mb' }));

// parse application/json
app.use(bodyParser.json({ limit: '1mb' }));

// parse application/text
//app.use(bodyParser.text({limit:1024 * 1024 * 100}));

// parse application/raw
//app.use(bodyParser.raw({limit:1024 * 1024 * 100}));

app.use(express.static(__dirname + '/../app'));

//----------------------------------------------------------------------------------------
// The cookie parser used before the session, this order is required for sessions to work.
// By default maxAge is null, meaning the cookie becomes a browser-session cookie, that is 
// when the user closes the browser the cookie (and session) will be removed.
// path --- cookie path
// expire --- absolute expiration date (Date object)
// maxAge --- relative max age of the cookie from when the client receives it (mill seconds)
// secure --- true or false
// domain --- domain for the cookie
// httpOnly --- true or false
//-----------------------------------------------------------------------------------------
app.use(cookieParser('S3CRE7', {maxAge: 1200*1000}));

//----------------------------------------------------------------------------------------
// Use conect-redis, session set up
// host 	--- Redis server host name
// prefix 	--- Key prefix default "sess"
// ttl		--- expiration in seconds
//----------------------------------------------------------------------------------------
app.use(session(
	{
	secret: 'salt pig hand',
	resave: false,
	saveUninitialized:true,
	cookie:{
		secure:false, // true will update session ID for every page refresh 
		maxAge: 5*60000	
	},
    proxy: false //true // if you do SSL outside of node.
//	store: new RedisStore({
//	host:'127.0.0.1',
//	port:6379,
//	prefix:'sess',
//	ttl:1200
//}),
}))


var apiURL = '/api';
var Traffic = require('./services/traffic');
var traffic = Traffic();
var User = require('./services/user');
var user = User();
var Program = require('./services/program');
var program = Program();
var School = require('./services/school');
var school = School();
var Consultant = require('./services/consultant');
var consultant = Consultant();

var Contact = require('./services/contact');
var contact = Contact();

var Message = require('./services/message');
var msg = Message();
var Upload = require('./services/upload');
var upload = Upload();

var Paypal = require('./services/pay');
var paypal = Paypal();

var Feedback = require('./services/feedback');
var feedback = Feedback();
//------------------------------------------------------------------------------------------
//	User module
//------------------------------------------------------------------------------------------
app.post(apiURL + '/login', user.login);
app.post(apiURL + '/getAccount', user.getAccount);
app.post(apiURL + '/getUserPhoto', user.getPhoto);
app.post(apiURL + '/renewToken', user.renewToken);
app.post(apiURL + '/signup', user.signup);
app.post(apiURL + '/getUsers', user.get);
app.post(apiURL + '/updateUser', function(req, rsp){
	var query = {};
	var updates = {};
	
	if(typeof req.body.query == 'string'){
		query = parser.parseQuery(JSON.parse(req.body.query));
	}else{
		query = parser.parseQuery(req.body.query);
	}
	
	if(typeof req.body.updates == 'string'){
		updates = JSON.parse(req.body.updates);
	}else{
		updates = req.body.updates;
	}
	
	user.updateOne(query, updates, function(err){
		if(err){
			return rsp.json({ 'success': false, 'error': err});
		}else{
			return rsp.json({ 'success': true, 'error': err});
		}
	});
});

app.post(apiURL + '/addUser', user.save);
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
app.post(apiURL + '/saveTraffic', traffic.save);
app.post(apiURL + '/getTrafficCount', traffic.count);

app.post(apiURL + '/getPrograms', program.get);
app.post(apiURL + '/getSchools', school.get);





app.post(apiURL + '/rmConsultant', consultant.remove);
app.post(apiURL + '/getConsultantDetail', consultant.getDetail);


app.post(apiURL + '/postMsg', msg.save);
app.post(apiURL + '/getMsg', msg.get);

app.post(apiURL + '/addContact', contact.save);
app.post(apiURL + '/getContact', contact.get);

app.post(apiURL + '/postFeedback', feedback.save);
app.post(apiURL + '/getFeedback', feedback.get);

app.post(apiURL + '/upload', upload.save);
app.post(apiURL + '/getDocs', upload.find);
app.post(apiURL + '/rmDoc', upload.remove);

app.post(apiURL + '/setCheckout', paypal.setCheckout);
app.post(apiURL + '/checkout', paypal.checkout);
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
http.createServer(app).listen(port, function(){
	console.log('Now serving the app at http://localhost:' + port + '/app');
});
