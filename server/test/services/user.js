var assert = require("assert")
var User = require("../../services/user");
var userService = User();

var UserModel = require("../../models/user");
var userModel = UserModel();

describe('Login', function(){
	
	beforeEach(function(){
	})

	afterEach(function(){
	})
	
	describe('login user with invalid inputs', function(){
		it('warns username only when provide empty username and valid password', function(done){
			var req = {body: {account:'', password:'zlk123Qw'}, session:{'logged in':false}};

			userService.login(req, function(errors, Error, doc){
				assert.equal(doc, null);
				assert.equal(errors.length, 1);						
				assert.equal(errors[0], Error.ACCOUNT_EMPTY);
				done();
			});
	    })
	    
	    it('warns 1 messages when provide valid username and empty password', function(done){
			var req = {body: {account:'zlk', password:''}, session:{'logged in':false}};
			userService.login(req, function(errors, Error, doc){
				assert.equal(doc, null);
				assert.equal(errors.length, 1);						
				assert.notEqual(errors.indexOf(Error.PASSWORD_MISMATCH), -1);
				done();
			});
	    })
	    
	    it('warns 1 messages when provide valid username and invalid password', function(done){
			var req = {body: {account:'zlk', password:'123'}, session:{'logged in':false}};
			userService.login(req, function(errors, Error, doc){
				assert.equal(doc, null);
				assert.equal(errors.length, 1);						
				assert.notEqual(errors.indexOf(Error.PASSWORD_MISMATCH), -1);
				done();
			});
	    })
	    
	    it('warns 1 messages when provide valid email and empty password', function(done){
			var req = {body: {account:'zlk@gmail.com', password:''}, session:{'logged in':false}};
			userService.login(req, function(errors, Error, doc){
				assert.equal(doc, null);
				assert.equal(errors.length, 1);						
				assert.notEqual(errors.indexOf(Error.PASSWORD_MISMATCH), -1);
				done();
			});
	    })
	    
	    it('warns 1 messages when provide valid email and invalid password', function(done){
			var req = {body: {account:'zlk@gmail.com', password:'123'},session:{'logged in':false}};
			userService.login(req, function(errors, Error, doc){
				assert.equal(doc, null);
				assert.equal(errors.length, 1);						
				assert.notEqual(errors.indexOf(Error.PASSWORD_MISMATCH), -1);
				done();
			});
	    })
	})
})


describe('Sign up', function(){
	
	beforeEach(function(done){
		var user_dict = {username:'zlk3', password:'zlk123Qw', email:'zlk3@gmail.com', phone:123456, role:1, created:1};
		userModel.remove({username:user_dict.username}, callback=function(err, doc){
			done();
	    });
	})

	afterEach(function(done){
		var user_dict = {username:'zlk3', password:'zlk123Qw', email:'zlk3@gmail.com', phone:123456, role:1, created:1};
		userModel.remove({username:user_dict.username}, callback=function(err, doc){
			done();
	    });
	})
	
	describe('create new user with invalid inputs', function(){
		it('warns username only when provide empty username and other valid inputs', function(done){
			var req = {body:{username:'', password:'zlk123Qw', email:'zlk@feizailin.ca', phone:123456, role:1, created:1}, session:{'logged in':false}};

			userService.signup(req, function(errors, Error, doc){
				assert.equal(doc, null);
				assert.equal(errors.length, 1);						
				assert.equal(errors[0], Error.USERNAME_EMPTY);
				done();
			});
	    })
	    
	    it('warns 2 messages when provide empty username, password and other valid inputs', function(done){
	    	var req = {body:{username:'', password:'', email:'zlk@feizailin.ca', phone:123456, role:1, created:1}, session:{'logged in':false}};

			userService.signup(req, function(errors, Error, doc){
				assert.equal(doc, null);
				assert.equal(errors.length, 2);						
				assert.notEqual(errors.indexOf(Error.USERNAME_EMPTY), -1);
				assert.notEqual(errors.indexOf(Error.PASSWORD_EMPTY), -1);
				done();
			});
	    })
	    
	    it('warns 3 messages when provide empty username, password, email and other valid inputs', function(done){
	    	var req = {body:{username:'', password:'', email:'', phone:123456, role:1, created:1}, session:{'logged in':false}};

			userService.signup(req, function(errors, Error, doc){
				assert.equal(doc, null);
				assert.equal(errors.length, 3);						
				assert.notEqual(errors.indexOf(Error.USERNAME_EMPTY), -1);
				assert.notEqual(errors.indexOf(Error.PASSWORD_EMPTY), -1);
				assert.notEqual(errors.indexOf(Error.EMAIL_EMPTY), -1);
				done();
			});
	    })
	    
	    it('warns 3 messages when provide empty username, email, simple password and other valid inputs', function(done){
	    	var req = {body:{username:'', password:'123', email:'', phone:123456, role:1, created:1}, session:{'logged in':false}};

			userService.signup(req, function(errors, Error, doc){
				assert.equal(doc, null);
				assert.equal(errors.length, 3);						
				assert.notEqual(errors.indexOf(Error.USERNAME_EMPTY), -1);
				assert.notEqual(errors.indexOf(Error.PASSWORD_TOO_SIMPLE), -1);
				assert.notEqual(errors.indexOf(Error.EMAIL_EMPTY), -1);
				done();
			});
	    })
	    
	    it('warns 1 messages when provide duplicated username and other valid inputs', function(done){
	    	var req = {body:{username:'zlk', password:'zlk123Qw', email:'zlk@feizailin.ca', phone:123456, role:1, created:1}, session:{'logged in':false}};

			userService.signup(req, function(errors, Error, doc){
				assert.equal(doc, null);
				assert.equal(errors.length, 1);						
				assert.notEqual(errors.indexOf(Error.USERNAME_EXISTS), -1);
				done();
			});
	    })
	    
	    it('warns 1 messages when provide duplicated email and other valid inputs', function(done){
	    	var req = {body:{username:'zlk3', password:'zlk123Qw', email:'zlk@gmail.com', phone:123456, role:1, created:1}, session:{'logged in':false}};

			userService.signup(req, function(errors, Error, doc){
				assert.equal(doc, null);
				assert.equal(errors.length, 1);						
				assert.notEqual(errors.indexOf(Error.EMAIL_EXISTS), -1);
				done();
			});
	    })
	    
	    it('warns 2 messages when provide duplicated username, email and other valid inputs', function(done){
	    	var req = {body:{username:'zlk', password:'zlk123Qw', email:'zlk@gmail.com', phone:123456, role:1, created:1}, session:{'logged in':false}};

			userService.signup(req, function(errors, Error, doc){
				assert.equal(doc, null);
				assert.equal(errors.length, 2);
				assert.notEqual(errors.indexOf(Error.USERNAME_EXISTS), -1);
				assert.notEqual(errors.indexOf(Error.EMAIL_EXISTS), -1);
				done();
			});
	    })
	    
	    it('warns 3 messages when provide duplicated username, email and other valid inputs', function(done){
	    	var req = {body:{username:'', password:'', email:'zlk@gmail.com', phone:123456, role:1, created:1}, session:{'logged in':false}};

			userService.signup(req, function(errors, Error, doc){
				assert.equal(doc, null);
				assert.equal(errors.length, 3);
				assert.notEqual(errors.indexOf(Error.USERNAME_EMPTY), -1);
				assert.notEqual(errors.indexOf(Error.PASSWORD_EMPTY), -1);
				assert.notEqual(errors.indexOf(Error.EMAIL_EXISTS), -1);
				done();
			});
	    })
	    
	    it('warns 3 messages when provide duplicated username, email and other valid inputs', function(done){
	    	var req = {body:{username:'zlk', password:'', email:'', phone:123456, role:1, created:1}, session:{'logged in':false}};

			userService.signup(req, function(errors, Error, doc){
				assert.equal(doc, null);
				assert.equal(errors.length, 3);
				assert.notEqual(errors.indexOf(Error.USERNAME_EXISTS), -1);
				assert.notEqual(errors.indexOf(Error.PASSWORD_EMPTY), -1);
				assert.notEqual(errors.indexOf(Error.EMAIL_EMPTY), -1);
				done();
			});
	    })
	})
	
	describe('create new user with valid inputs and be found', function(){
	    it('warns 1 messages when provide valid inputs', function(done){
	    	var req = {body:{username:'zlk3', password:'zlk123Qw', email:'zlk3@gmail.com', phone:123456, role:1, created:1}, session:{'logged in':false}};

			userService.signup(req, function(errors, Error, doc){
				assert.notEqual(doc, null);
				assert.equal(errors.length, 1);
				assert.notEqual(errors.indexOf(Error.NONE), -1);

				userModel.findOne(req.body, function(err, doc){
					assert.equal(doc.username, req.body.username);
					done();
				});
			});
	    })
	})
})


/*
	describe('create and find one document', function(){
		it('should insert successful',function(done){
			users.save({'username':'zlk'}, function(err, doc){
				assert.equal(doc.username, 'zlk');
				console.log('save called');
				
				users.findOne({'username':'zlk'}, function(err, doc2){
					assert.equal(err, null);
					assert.equal(doc2.username, 'zlk');
					console.log('findOne called');
					
					users.update({username:'zlk'}, {$set:{username:'zlk2'}}, {upsert:false, multi:true}, function(err, doc3){
						assert.equal(err, null);
						assert.equal(doc3.n, 1);
						console.log(doc3);
						done();
					})
				})
			})
		})
	})
*/