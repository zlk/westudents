var assert = require("assert")
var DB = require("../db");

describe('Database', function(){
	
	describe('When use default param to connect and get Database', function(){
		it('should return db', function(){
			var db = new DB();
			var d = db.getDatabase();
			assert.notEqual(d, null);
			
			var users = db.getCollection('users');
			assert.notEqual(users, null);
		});
	});
	
	describe('Generate a new object id', function(){
		it('should return object id', function(done){
			var db = new DB();
			assert.notEqual(db.genObjectId(), null);
			done();
		});
	});
	
});