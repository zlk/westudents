//----------------------------------------------------
// Author:	Martin.Zhang
// Date:  	August 13 2015
// All right reserved.
//----------------------------------------------------

'use strict';


var FS = require('fs');
var Config = require('./config');

module.exports = {
		
		log:function(msg){
			var dt = new Date();
			dt = dt.toISOString().split('.')[0];
			FS.appendFileSync(Config.logFile, '[' + dt + ']' + msg + '\n');
			console.log('[' + dt + ']' + msg);
		},
		
	saveFile : function (fname, fdata, callback){
		// If there is no image exist create a new one
		// fname --- full name of the file to be save on the server
		// fdata --- file stream get from document files[0] in html5
		// callback --- non arguments function
	    fs.open(fname, 'w+', function(err, fd){
	    	var datas = fdata.split(',');
	    	var data = datas[1];
	    	var buf = new Buffer(data, 'base64');
	    	
	        //var info = datas[0];
	        //_type, self.encoding = info.split(';')
	        //self.file_type, self.file_ext = _type.split(r'/')
	        
	    	// write image to file
	    	fs.write(fd, buf, 0, buf.length, 0, function(err, written, buffer){
	    		console.log('write image to server successful.');
	
	    		if(callback == undefined){
	    			callback = function(){
	    				console.log('saveImage callback is undefined');
	    			}
	    		}
	    		
	    		fs.close(fd, callback);
	    	});
	    });
	}//End of saveFile
}