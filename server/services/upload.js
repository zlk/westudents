//----------------------------------------------------
// Author:	Martin.Zhang
// Date:  	August 13 2015
// All right reserved.
//----------------------------------------------------

'use strict';

var FS = require('fs');
var FSE = require('fs-extra');
var DB = require('../db.js');
var Logger = require('../logger.js');
var User = require('./user');
var user = User();
var Utils = require('./utils');
var ut = Utils();
var Busboy = require('busboy');
var Cfg = require('../config');
var gm = require('../../node_modules/gm');
module.exports = function(){
	var _name = 'docs';
	var _db = new DB();
	var _collection = _db.getCollection(_name);
	//------------------------------------------------------------------
	// If there is no image exist create a new one
	// Arguments:
	// 	fdata --- file stream get from document files[0] in html5
	//------------------------------------------------------------------
	// file type / file extension ; encoding, data
	function parseFileData( fdata ){
		var datas = fdata.split(','); 
		var info = datas[0].split(';');
		var finfo = info[0].split('/');
		
		//var regex = /^data:.+\/(.+);base64,(.*)$/;
		//var matches = fdata.match(regex);
		//var ext = matches[1];
		//var data = matches[2];
		var buffer = new Buffer(datas[1], 'base64');
		return {file_ext:finfo[1], buf:buffer};
	}
	
	function saveFile(fname, path, buf){
		//------------------------------------------------------------------
		// If there is no image exist create a new one
		// Arguments:
		// 	fpath --- full name of the file to be save on the server
		// 	buf --- base 64 decoded image buffer
		//------------------------------------------------------------------
		/*
		FS.open(fname, 'w+', function(err, fd){
	    	FS.write(fd, buf, 0, buf.length, 0, function(err, written, buffer){
	    		console.log('write image to server successful.');
	    		FS.close(fd, callback);
	    		});
	      	});*/
		if (!FS.existsSync(path)) {
		    FSE.mkdirsSync(path);
		}
		var fpath = path + '/' + fname;
		var fd = FS.openSync(fpath, 'w+');
		//FS.writeSync(fd, buf, 0, buf.length, 0);
		FS.writeFileSync(fpath, buf);
		FS.closeSync(fd);
	}
	
	function getFileData(fname, path){
		var fpath = path + '/' + fname;
		var fd = FS.openSync(fpath, 'r');
		var buf = FS.readFileSync(fpath);
		FS.closeSync(fd);
		return buf;
	}
	
	function savePhoto(d, callback){
		var folderPath =  'photos/' + d.username;
		var s = d.fname.split('.');
		var ext = s[s.length - 1];
		var dt = new Date();
		var surfix = dt.toISOString().replace(/\:/g,'-');
    	var fname = 'p_' + surfix + '.' + ext;
    	var fpath = folderPath + '/' + fname;
		
		if (!FS.existsSync(Cfg.storageRoot + folderPath)) {
		    FSE.mkdirsSync(Cfg.storageRoot + folderPath);
		}
		
		user.updateOne({'username': d.username}, {'$set':{photo:fpath}}, function(){
			
		});
		
		return fpath;
	}
	
	function saveDoc(d, callback){
		var folderPath =  'docs/' + d.username;
		var s = d.fname.split('.');
		var ext = s[s.length - 1];
		var dt = new Date();
		var surfix = dt.toISOString().replace(/\:/g,'-');
		var fname = s[0] + '_' + surfix + '.' + ext;
    	var fpath = folderPath + '/' + fname;
		
		if (!FS.existsSync(Cfg.storageRoot + folderPath)) {
		    FSE.mkdirsSync(Cfg.storageRoot + folderPath);
		}
		
		var info = { 'type': d.type, 'username': d.username, 'fpath': fpath, 'fname': d.fname, 'created': dt.toISOString() };

		 _collection.save(info, callback);
		 
		return fpath;
	}
	
	return {
		//--------------------------------------------------------------------------------------
		// find
		// Arguments:
		// 		query --- query object, eg
		// 		callback --- function(err, docs)
		//--------------------------------------------------------------------------------------
		find: function(req, rsp){
			var query = {};
			_collection.find(query, function(err, docs){
				return rsp.json({ 'success': true, 'docs': docs});
			});
		},
		
		remove: function(req, rsp){
			ut.checkToken(req, rsp, function(d){
				var query = {_id:_db.toObjectId(req.body._id)};
				_collection.remove(query, function(err, docs){
					return rsp.json({ 'success': true, 'docs': docs});
				});
			})
		},
		
		//--------------------------------------------------------------------------------------
		//	save() http post handler
		// 	If ip exist, increase count; if not exist, give position and count, then save
		//--------------------------------------------------------------------------------------
		save: function(req, rsp){
			var busboy = new Busboy({ headers: req.headers });
			var username = '', folderPath, filePath, info;
			
			//-------------------------------------------------------------------------------
			// iterate through all the fields of formData from http.post
			//-------------------------------------------------------------------------------
			busboy.on('field', function(fieldName, val, fieldNameTruncated, valTruncated) {
				if(fieldName == 'info'){
					info = JSON.parse(val);
					
			    	if(info.username != ""){
			    		if(info.type=='photo'){
			    			filePath = savePhoto(info);
			    		}else if(['Resume', 'Personal Statement', 'Reference Letter'].indexOf(info.type) > -1){
			    			filePath = saveDoc(info);
			    		}
			    	}
				}
			}); //end of on field
			
			// This event handler will be called multiple times
			busboy.on('file', function(fieldName, file, fileName, encoding, mimeType) {
			    if(file!=null && fileName!=''){
			    	/*
			    	savePictureInfo(_id, filePath, itemId, order, _size, function(err, pic){
	        			console.log('[success] --- Save picture to database.')
	        		});
			    	*/
			    	
			    	// Save file to server
		            var fstream = FS.createWriteStream(Cfg.storageRoot + filePath);
		            file.pipe(fstream);		            
			    	
		            /*
		            fstream.on('end', function() {
		                console.log("Stream EOF:" + fieldName);
		            });
		            
		            fstream.on('close', function() {
		                console.log("Stream CLOSE:" + fieldName);
		            });
		            
		            fstream.on('error', function(err) {
		                console.log("Stream ERROR:" + err + fieldName);
		            });
		            */
		            file.on('data', function(data) {
		                //console.log('File OnData:' + fieldName);// + '  ' + data.length + ' bytes');
		                console.log('File OnData:');
		            });
		            
		            file.on('end', function() {
		            	console.log('File EOF:');
				    	//var fileExt = fileName.split('.').pop();
				    	//var fname = fieldName + '.' + fileExt;
				    	//console.log('File EOF:' + fieldName + ' ' + folderPath + '  ' + fname);
		            	
		        		//var fPath = folderPath + '/f' + order + '.' + fileExt;
		            });
			    }			    
			}); // end of busboy.on('file')
			
			busboy.on('end', function(){
				console.log('Busboy EOF');
			});
			
			busboy.on('finish', function(){
				console.log('Busboy OnFinish');
				rsp.end();
				
				// resize image to thumbnail
				if(info.type=='photo'){
					//var ret = filePath.split('.');
					//var spath = ret[0] + '-s.' + ret[1];
					var THUMB_WIDTH = info.width;
					var THUMB_HEIGHT = info.height; // 4 : 3
					//var ratio = THUMB_WIDTH / pic.width;
					
					gm(Cfg.imageRootFolder + filePath).thumb(THUMB_WIDTH, THUMB_HEIGHT, Cfg.imageRootFolder + filePath, 100,
							function (err) {
								if (!err) console.log('create thumbnail done');
							});
				}
				//return res.json({pictures:'good'});
				//res.writeHead(200, { 'Connection': 'close' });
			    //res.end("That's all folks!");
			});
			
			req.on('end', function(){
				console.log('Request EOF');
			});
			
			return req.pipe(busboy);
			//save path, user
			//save file
			/*var body = req.body;
			 _collection.save(body, function(err, doc){
				 return rsp.json({ success: true, message: doc});
			 });
			 
				folderPath =  'images/' + itemId;
				if (!FS.existsSync(SERVER_PATH + folderPath)) {
				    FSE.mkdirsSync(SERVER_PATH + folderPath);
				}
			*/
		}
	
		//----------------------------------------------------------------------
		// Use busboy to parse the http.post formData.
		// http.post formData {id:item_id, f0:blob0, f1:blob1, f2:blob2 ...}
		//----------------------------------------------------------------------
	}
}
