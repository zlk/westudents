//----------------------------------------------------
// Author:	Martin.Zhang
// Date:  	August 13 2015
// All right reserved.
//----------------------------------------------------

'use strict';

var _us = require("../../node_modules/underscore/underscore-min");
var DB = require('../db.js');
var Logger = require('../logger.js');
var Utils = require('./utils');
var utils = Utils();

module.exports = function(){
	
	var _name = 'traffics';
	var _db = new DB();
	var _collection = _db.getCollection(_name);
	
	//--------------------------------------------------------------------------------------
	// If ip exist, increase count; if not exist, give position and count, then save
	//		q -- {ip:x, sessionID:y}
	// 		callback --- function(err, doc)
	//--------------------------------------------------------------------------------------
	function _save(q, callback){
		_collection.findOne( {ip:q.ip}, function(error, doc){
			
			if(doc){
				//Logger.log('ip:' + q.ip + ' net sessionID:' + q.sessionID);
				//Logger.log('ip:' + q.ip + ' db sessionID:' + doc.sessionID + 'db count:' + doc.count);
				
				if(doc.sessionID != q.sessionID){
					doc.count += 1;
					doc.sessionID = q.sessionID;
					
					_collection.save(doc, function(err, s){
						if(callback){
							callback(err, s);
						}
					});
				}else{
					if(callback){
						callback(error, doc);
					}
				}
			}else{
				_collection.count(function(err, pos){
					var t = {ip: q.ip, sessionID:q.sessionID, count:1, position: pos};
					_collection.save(t, function(err, s){
						if(callback){
							callback(err, s);
						}
					});
				});
			}
		});
	}
	
	return {
		//--------------------------------------------------------------------------------------
		//	save() http post handler
		// 	If ip exist, increase count; if not exist, give position and count, then save
		//--------------------------------------------------------------------------------------
		save: function(req, rsp){
			var ip = null;
			
			if(req.headers.hasOwnProperty('x-forwarded-for')){
				ip = req.headers['x-forwarded-for'];
			}else{
				ip = req.ip;
			}
			
			_save({'ip':ip, 'sessionID': req.sessionID}, function(err, doc){
				return rsp.json({'traffic':doc});
			});
		},
		
		count: function(req, rsp){
			_collection.find({}, function(err, docs){
				var sum = 0;
				_us.each(docs, function(v, i){
					sum += v.count;
				});
				return rsp.json({'count':sum});
			});
		},
		
		//--------------------------------------------------------------------------------------
		// find
		// Arguments:
		// 		query --- query object, eg
		// 		callback --- function(err, docs)
		//--------------------------------------------------------------------------------------
		find: function(query, callback){
			_collection.find(query, callback);
		},
		
		//--------------------------------------------------------------------------------------
		// findOne
		// Arguments:
		// 		query --- query object, eg. 
		// 		callback --- function(err, doc)
		//--------------------------------------------------------------------------------------
		findOne : function(query, callback){
			_collection.findOne(query, callback);
		}
		

		
	}
}
