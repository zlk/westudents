//----------------------------------------------------
// Author:	Martin.Zhang
// Date:  	August 13 2015
// All right reserved.
//----------------------------------------------------

'use strict';

var DB = require('../db.js');
var Logger = require('../logger.js');
var Utils = require('./utils');
var Config = require('../config')
var utils = Utils();
var ut = Utils();

var https = require('https');
var querystring = require('querystring');
var urlParser = require('url');

module.exports = function(){
	
	var params = {
    		USER:'likzhang-facilitator_api1.gmail.com',
            PWD:'R7F2N67PRH6WLJXT',
            SIGNATURE:'APmj1Fa-M.dAK-o-C.FzXvUzTkrrAPNCwzO0Oqgg2YPiPxyBdsq8ZoQ0',
			VERSION: '124.0'	
	};
	
	var TIMEOUT = 10000; // 10s
	
	function request(url, method, data, callback) {
		var params = querystring.stringify(data);

		if (method === 'GET') {
			url += '?' + params;
		}

		var uri = urlParser.parse(url);
		var headers = {};

		headers['Content-Type'] = method === 'POST' ? 'application/x-www-form-urlencoded' : 'text/plain';
		headers['Content-Length'] = params.length;

		var options = { 
			protocol: uri.protocol, 
			auth: uri.auth, 
			method: method || 'GET', 
			hostname: uri.hostname, 
			port: uri.port, 
			path: uri.path, 
			agent: false, 
			headers: headers 
		};

		var req = https.request(options, function(res) {
			var buffer = '';

			res.on('data', function(chunk) {
				buffer += chunk.toString('utf8');
			});

			req.setTimeout(TIMEOUT, function() {
				callback(new Error('timeout'), null);
			});

			res.on('end', function() {
				var error = null;
				var data = '';

				if (res.statusCode > 200) {
					error = new Error(res.statusCode);
					data = buffer;
				} else {
					data = querystring.parse(buffer);
				}

				callback(error, data);
			});
		});

		if (method === 'POST') {
			req.end(params);
		} else {
			req.end();
		}
	}
	
	// Total cost to the transaction to the buyer, cannot exceed 10,000 USD
	function setExpressCheckout(url, amount, successCb, failCb) {
		params.METHOD = 'SetExpressCheckout';
		params.PAYMENTREQUEST_0_PAYMENTACTION = 'Sale';
		params.PAYMENTREQUEST_0_AMT = amount;
		params.RETURNURL = "http://westudents.ca/#/booking";
		params.CANCELURL = "http://westudents.ca";
		
		//params.SOLUTIONTYPE = onlyPayPalUsers === true ? 'Mark' : 'Sole';
		//params.PAYMENTREQUEST_0_AMT = amount;
		//params.PAYMENTREQUEST_0_DESC = description;
		//params.PAYMENTREQUEST_0_CURRENCYCODE = currencyCode;
		//params.PAYMENTREQUEST_0_INVNUM = invoiceNumber;
		//params.PAYMENTREQUEST_0_CUSTOM = invoiceNumber + '|' + params.PAYMENTREQUEST_0_AMT + '|' + currency;
		//params.PAYMENTREQUEST_0_PAYMENTACTION = 'Sale';
		//params.PAYMENTREQUEST_0_ITEMAMT = amount;

		request(url, 'POST', params, function(err, data) {
			if (err || data.ACK != 'Success') {
				if(failCb)
					failCb(err, data);
				return;
			}
			
			if(successCb)
				successCb(null, data);
		});
	}
	
	function getExpressCheckoutDetail(url, token, successCb, failCb) {
		params.TOKEN = token; 
		params.METHOD = 'GetExpressCheckoutDetails';
		request(url, 'POST', params, function(err, data) {
			if (err || data.ACK != 'Success') {
				if(failCb)
					failCb(err, data);
				return;
			}
			
			if(successCb)
				successCb(null, data);
		});
	}
	
	function doExpressCheckout(url, amount, payerID, token, successCb, failCb) {
		params.METHOD = 'DoExpressCheckoutPayment';
		params.PAYERID = payerID;
		//params.PAYMENTREQUEST_0_PAYMENTACTION = 'Sale';
		//params.PAYMENTREQUEST_0_AMT = amount;
		params.TOKEN = token;
		params.PAYMENTREQUEST_0_AMT = amount; //data.PAYMENTREQUEST_0_AMT;
		//params.PAYMENTREQUEST_0_CURRENCYCODE = 'CAD';//data.PAYMENTREQUEST_0_CURRENCYCODE;
		//params.PAYMENTREQUEST_0_ITEMAMT = data.PAYMENTREQUEST_0_ITEMAMT;
		//params.RETURNURL = "http://localhost:5003/#/consultants";
		//params.CANCELURL = "http://localhost:5003";

		request(url, 'POST', params, function(err, data) {
			if (err || data.ACK != 'Success') {
				if(failCb)
					failCb(err, data);
				return;
			}
			
			if(successCb)
				successCb(null, data);
		});
	}
	
	return {
		setCheckout : function( req, rsp ){
			var url = 'https://api-3t.sandbox.paypal.com/nvp';
			var amount = req.body.amount;
			
			setExpressCheckout(url, amount, function(err, data){
				return rsp.json(data);
			});
		},
	
		checkout : function( req, rsp ){
			var url = 'https://api-3t.sandbox.paypal.com/nvp';
			var amount = req.body.amount;
			var token = req.body.token;
			var payerID = req.body.payerID;
			
			doExpressCheckout(url, amount, payerID, token, function(err, data){
				return rsp.json(data);
			});
		}
	
		/*
		$scope.goPaypal = function(){
        	$scope.getExpressCheckout(function(data){
        		$location.url = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" + data.TOKEN;
        	})
        }
        
     	//TOKEN
        //ACK -- Success
        //TIMESTAMP -- YYYY/MM/DDTHH-MM-SSZ
        $scope.getExpressCheckout = function(callback){
			$http.post('https://api-3t.sandbox.paypal.com/nvp', {
				USER:$scope.paypal.username,
				PWD:$scope.paypal.password,
				SIGNATURE:$scope.paypal.signature,
				VERSION:'124.0',
				PAYMENTREQUEST_0_PAYMENTACTION: 'Personal Statement Review',
				PAYMENTREQUEST_0_AMT: 200,
				RETURNURL:"http://localhost:5003/#/consultants",
				CANCELURL:"http://localhost:5003",
				METHOD:'SetExpressCheckout'
			})
			.success(function(data, status, headers, config) {
				if(callback){
					callback(data);
				}
			})
			.error(function(data, status, headers, config) {
				if(callback){
					callback(data);
				}
				console.log('[failed] --- count traffic');
			});
        }
		
		
		getExpressCheckoutDetails = function(token, doPayment, callback) {
			var p = $.extend({}, params, {TOKEN:token, METHOD:'GetExpressCheckoutDetails'});

			self.request(self.url, 'POST', params, function(err, data) {
				if (err) {
					callback(err, data);
					return;
				}

				if (!doPayment) {
					return callback(null, data);
				}

				var params = self.params();
				params.PAYMENTACTION = 'Sale';
				params.PAYERID = data.PAYERID;
				params.TOKEN = token;
				params.PAYMENTREQUEST_0_AMT = data.PAYMENTREQUEST_0_AMT;
				params.PAYMENTREQUEST_0_CURRENCYCODE = data.PAYMENTREQUEST_0_CURRENCYCODE;
				params.PAYMENTREQUEST_0_ITEMAMT = data.PAYMENTREQUEST_0_ITEMAMT;
				params.METHOD = 'DoExpressCheckoutPayment';

				self.request(self.url, 'POST', params, function(err, data2) {
					if (err) {
						callback(err, data2);
						return;
					}

					if (data.ACK  !== 'Success') {
						return callback(new Error('Error DoExpressCheckoutPayment'), data2);
					}


					// Combine results of getExpressCheckout and DoExpress checkout payment.
					callback(null, _.extend(data, data2));
				});
			});

			return self;
		};
		*/
	}
}