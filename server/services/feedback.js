//----------------------------------------------------
// Author:	Martin.Zhang
// Date:  	Jan 13 2016
// All right reserved.
//----------------------------------------------------

'use strict';

var DB = require('../db.js');
var Logger = require('../logger.js');
var Utils = require('./utils');
var ut = Utils();

module.exports = function(){
	
	var _name = 'feedbacks';
	var _db = new DB();
	var _collection = _db.getCollection(_name);
	
	return {
		
		//--------------------------------------------------------------------------------------
		//	save() http post handler
		// 	If ip exist, increase count; if not exist, give position and count, then save
		//--------------------------------------------------------------------------------------
		save: function(req, rsp){
			var body = req.body;
			 _collection.save(body, function(err, doc){
				 return rsp.json({ success: true, message: doc});
			 });
		},
		
		get: function(req, rsp){
			ut.checkToken(req, rsp, function(d){
				_collection.find({}, function(err, docs){
					return rsp.json({ success: true, feedbacks: docs});
				});
			})
		},
		//--------------------------------------------------------------------------------------
		// find
		// Arguments:
		// 		query --- query object, eg
		// 		callback --- function(err, docs)
		//--------------------------------------------------------------------------------------
		find: function(query, callback){
			_collection.find(query, callback);
		},
		
		//--------------------------------------------------------------------------------------
		// findOne
		// Arguments:
		// 		query --- query object, eg. 
		// 		callback --- function(err, doc)
		//--------------------------------------------------------------------------------------
		findOne : function(query, callback){
			_collection.findOne(query, callback);
		}
	}
}
