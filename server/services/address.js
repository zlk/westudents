
var AddressModel = require("../model/address");
var addressModel = new AddressModel();

module.exports = function(db){
	var collection = db.getCollection('addresses');
	
	return {
		//-------------------------------------------------------
		// Save address if not exist
		// Arguments:
		// 	addr     --- { unit, street, city, province, country }
		//	callback --- function(err, doc){}
		//-------------------------------------------------------
		save: function(addr, callback){
			addressModel.save(addr, callback);
		},
		
		//------------------------------------------------
		// query -- { unit, street, city_id }
		// callback - function(err, doc)
		//------------------------------------------------
		find : function(query, callback){
			addressModel.find(query, callback);	
		},
		
		//------------------------------------------------
		// query -- { unit, street, city_id }
		// callback - function(err, doc)
		//------------------------------------------------
		remove: function(query, callback){
			addressModel.remove(query, callback);
			//collection.remove(query, false, callback);//remove all
		}
		
	}
}