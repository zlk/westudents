//----------------------------------------------------
// Author:	Martin.Zhang
// Date:  	August 13 2015
// All right reserved.
//----------------------------------------------------

'use strict';

var DB = require('../db.js');
var Logger = require('../logger.js');
var Utils = require('./utils');
var ut = Utils();

module.exports = function(){
	
	var _name = 'contacts';
	var _db = new DB();
	var _collection = _db.getCollection(_name);
	
	return {
		
		//--------------------------------------------------------------------------------------
		//	save() http post handler
		// client, consultant --- user name
		//--------------------------------------------------------------------------------------
		save: function(req, rsp){
			ut.checkToken(req, rsp, function(d){
				if(d.username){
					var c = req.body.contact;
					if(c){
						 _collection.findOne({client:c.client, consultant:c.consultant}, function(err, doc){
							 if(!doc){
								 _collection.save(c, function(err, doc){
									 return rsp.json({ success: true});
								 });
							 }else{
								 return rsp.json({success:false});
							 }
						 });
					}else{
						return rsp.json({success:false});
					}
				}else{
					return rsp.json({ success: false});
				}
			});

		},
		
		get: function(req, rsp){
			ut.checkToken(req, rsp, function(d){
				if(d.username){
					if(req.body.query){
						_collection.find(req.body.query, function(err, docs){
							return rsp.json({ success: true, contacts: docs});
						});
					}else{
						return rsp.json({success:false, contacts:[]});
					}
				}else{
					return rsp.json({success:false, contacts:[]});
				}
			})
			
		},
		//--------------------------------------------------------------------------------------
		// find
		// Arguments:
		// 		query --- query object, eg
		// 		callback --- function(err, docs)
		//--------------------------------------------------------------------------------------
		find: function(query, callback){
			_collection.find(query, callback);
		},
		
		//--------------------------------------------------------------------------------------
		// findOne
		// Arguments:
		// 		query --- query object, eg. 
		// 		callback --- function(err, doc)
		//--------------------------------------------------------------------------------------
		findOne : function(query, callback){
			_collection.findOne(query, callback);
		}
	}
}
