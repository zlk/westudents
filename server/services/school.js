//----------------------------------------------------
// Author:	Martin.Zhang
// Date:  	December 29 2015
// All right reserved.
//----------------------------------------------------
'use strict';

var DB = require('../db.js');
var Logger = require('../logger.js');
var Utils = require('./utils');
var ut = Utils();

module.exports = function(){	
	var _name = 'schools';
	var _db = new DB();
	var _collection = _db.getCollection(_name);
	
	return {
//		gen: function(){
//			_collection.find({}, function(err, schools){
//				for(var i in schools){
//					var s = schools[i];
//					
//					var _col = _db.getCollection('cities');
//					_col.findOne({'_id': s.city_id)}, function(err, c){
//						s['province'] = c.province;
//						s['country'] = c.country;
//						_collection = _db.getCollection('schools');
//						_collection.update({'_id': s._id}, s, {'upsert':false, multi: false});
//					});
//				}
//			})
//		},
		//--------------------------------------------------------------------------------------
		//	save() http post handler
		// 	If ip exist, increase count; if not exist, give position and count, then save
		//--------------------------------------------------------------------------------------
		save: function(req, rsp){
			var body = req.body;
			 _collection.save(body, function(err, doc){
				 return rsp.json({ success: true, school: doc});
			 });
		},
		
		// type:1 universities, type:2 colleges
		get: function(req, rsp){
			//ut.checkToken(req, rsp, function(d){
				_collection.find(req.body, function(err, docs){
					return rsp.json({ success: true, schools: docs});
				});
			//})
		},
		
		//--------------------------------------------------------------------------------------
		// find
		// Arguments:
		// 		query --- query object, eg
		// 		callback --- function(err, docs)
		//--------------------------------------------------------------------------------------
		find: function(query, callback){
			_collection.find(query, callback);
		},
		
		//--------------------------------------------------------------------------------------
		// findOne
		// Arguments:
		// 		query --- query object, eg. 
		// 		callback --- function(err, doc)
		//--------------------------------------------------------------------------------------
		findOne : function(query, callback){
			_collection.findOne(query, callback);
		}
	}
}
