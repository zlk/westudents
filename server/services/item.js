var Validator = require('./validator')
var Item = require('../models/item');
var Async = require('async');
var DB = require("../db");
var db = DB();
var Parser = require('../query_parser');
var parser = Parser();
var Config = require('../config')
var FS = require('fs');

var ItemPicture = require("../models/item_picture");

var AddressModel = require("../models/address");
var CityModel = require("../models/city");

var pic_model = ItemPicture(db);
var item_model = Item();

var city_model = new CityModel();
var addr_model = new AddressModel();

var v = Validator();

var ItemError = {
		NONE:0,
		EMPTY:1,
		NOT_NUMBER:2,
		INVALID_CHAR:3,
		NOT_EMAIL:4
	};


module.exports = function(){
	// Deprecated function
	function addCitiesQuery(query, callback){
		// query --- city query
		// callback --- function(err, cities)
		city_model.find(query, function(err, cities){
			var cityIds = [];
			var cityQuery = null;
			
			if(cities !=null){
				for(var i=0; i<cities.length; i++){
					cityIds.push(cities[i]._id);
				}
			}
			
			if( cityIds.length!=0){
				cityQuery = {$in:cityIds};
			}
			
			if(callback != null){
				callback(cityQuery);
			}
		});
	}
	
	// Deprecated
	function getPicturePath(pictures, order){
		for( var i=0; i<pictures.length; i++){
			if(pictures[i].order == order){
				return pictures[i].path;
			}
		}
		return null;
	}
	
	return {
		//--------------------------------------------------------------------------------------
		// findItems --- used in city
		// Arguments:
		// req --- http req object with body.query and body.updates fields. if the req is from
		//	mobile phone, it's a json string, if it's from web, it's a json object
		//	eg. { body: { query:{_id:xxx}, updates: { $set:{status: 0, created:ISODate() }}}}
		// callback --- function(err, doc)
		//--------------------------------------------------------------------------------------
		findItems: function(req, callback){

		},
		//--------------------------------------------------------------------------------------
		// updateItem by query _id
		// Arguments:
		// req --- http req object with body.query and body.updates fields. if the req is from
		//	mobile phone, it's a json string, if it's from web, it's a json object
		//	eg. { body: { query:{_id:xxx}, updates: { $set:{status: 0, created:ISODate() }}}}
		// callback --- function(err, doc)
		//--------------------------------------------------------------------------------------
		updateItem: function(req, callback){
			var query = {};
			var updates = {};
			
			if(typeof req.body.query == 'string'){
				query = parser.parseQuery(JSON.parse(req.body.query));
			}else{
				query = parser.parseQuery(req.body.query);
			}
			
			if(typeof req.body.updates == 'string'){
				updates = JSON.parse(req.body.updates);
			}else{
				updates = req.body.updates;
			}
			
			if(Object.keys(updates['$set']).length != 0){
				item_model.updateOne(query, updates, callback);
			}
			
		},

		//--------------------------------------------------------------------------------------
		// deleteItem by query _id
		// Arguments:
		// req --- http req object with fields. 
		//	if the req is from mobile phone, it's a json string. 
		//	if it's from web, it's a json object
		//	eg. { body: { query:{_id:xxx}, updates: { $set:{status: 0, created:ISODate() }}}}
		// callback --- function(err, doc)
		//--------------------------------------------------------------------------------------
		deleteItem: function(req, callback){
			var query = {};
			
			if(typeof req.query == 'string'){
				query = parser.parseQuery(JSON.parse(req.query));
			}else{
				query = parser.parseQuery(req.query);
			}
			
			// Delete pictures and links
			var SERVER_PATH = Config.imageRootFolder;
			var itemId = query._id;
			var dirPath = SERVER_PATH + itemId; 
			FS.exists(dirPath, function(bExists){
				if(bExists){
					FS.unlink(dirPath, function(){});
				}	
			});
			
			pic_model.remove({item_id:db.toObjectId(itemId)}, true, function(err, pic){
				item_model.remove({_id:db.toObjectId(itemId)}, function(err, doc){
					if(callback){
						callback(err, doc);
					}
				})
			});
		},
		
		//--------------------------------------------------------------------
		// Save item with _id field
		// Arguments:
		//	callback --- function(err, doc){}
		//	item 	 --- { _id*, title, description, price, created, property_type, unit, street, postcode, city, province, country }
		//--------------------------------------------------------------------
		save: function(req, callback){
			var item = req.body;
			
			var errCodes = {};
			
			// Validate input fields
			if(v.isEmpty(item.province)){
				errCodes['province'] = ItemError.EMPTY;
			}else{
				if(v.hasInvalidChar(item.province)){
					errCodes['province'] = ItemError.INVALID_CHAR;
				}
			}
			
			if(v.isEmpty(item.city)){
				errCodes['city'] = ItemError.EMPTY;
			}else{
				if(v.hasInvalidChar(item.city)){
					errCodes['city'] = ItemError.INVALID_CHAR;
				}
			}
			
			if(v.hasInvalidChar(item.unit)){
				errCodes['unit'] = ItemError.INVALID_CHAR;
			}
			
			if(v.isEmpty(item.street)){
				errCodes['street'] = ItemError.EMPTY;
			}else{
				if(v.hasInvalidChar(item.street)){
					errCodes['street'] = ItemError.INVALID_CHAR;
				}
			}
			/*
			if(v.hasInvalidChar(item.postcode)){
				errCodes['postcode'] = ItemError.INVALID_CHAR;
			}
			
			if(v.isEmpty(item.title)){
				errCodes['title'] = ItemError.EMPTY;
			}else{
				if(v.hasInvalidChar(item.title)){
					errCodes['title'] = ItemError.INVALID_CHAR;
				}
			}
			*/
			if(v.isEmpty(item.description)){
				errCodes['description'] = ItemError.EMPTY;
			}
			// Description field will do Utf-8 encoding and html encoding
			/*else{
				if(v.hasInvalidChar(item.description)){
					errCodes['description'] = ItemError.INVALID_CHAR;
				}
			}*/
			
			if(v.isEmpty(item.price)){
				errCodes['price'] = ItemError.EMPTY;
			}else{
				if(!v.isNumber(item.price)){
					errCodes['price'] = ItemError.NOT_NUMBER;
				}
			}
			
			if(v.isEmpty(item.nbedrooms)){
				errCodes['nbedrooms'] = ItemError.EMPTY;
			}else{
				if(!v.isNumber(item.nbedrooms)){
					errCodes['nbedrooms'] = ItemError.NOT_NUMBER;
				}
			}
			/*
			if(v.isEmpty(item.nbaths)){
				errCodes['nbaths'] = ItemError.EMPTY;
			}else{
				if(!v.isNumber(item.nbaths)){
					errCodes['nbaths'] = ItemError.NOT_NUMBER;
				}
			}
			
			if(v.isEmpty(item.nparkings)){
				errCodes['nparkings'] = ItemError.EMPTY;
			}else{
				if(!v.isNumber(item.nbaths)){
					errCodes['nparkings'] = ItemError.NOT_NUMBER;
				}
			}
			*/
			
			if(!v.isEmpty(item.email)){
				if(v.hasInvalidChar(item.email)){
					errCodes['email'] = ItemError.INVALID_CHAR;
				}/*else{
					if(!v.isEmail(item.email)){
						errCodes['email'] = ItemError.NOT_EMAIL;
					}
				}*/
			}
			
			if(!v.isEmpty(item.phone)){
				if(v.hasInvalidChar(item.phone)){
					errCodes['phone'] = ItemError.INVALID_CHAR;
				}
			}
			
			if(Object.keys(errCodes).length == 0){
				item_model.save(req.body, function(err, doc){
					if(callback){
						callback({}, doc);
					}	
				});
			}else{
				if(callback){
					callback(errCodes, null);
				}
			} 	
		}
	}
}
