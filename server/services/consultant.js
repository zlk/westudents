//----------------------------------------------------
// Author:	Martin.Zhang
// Date:  	December 29 2015
// All right reserved.
//----------------------------------------------------
'use strict';

var DB = require('../db.js');
var Parser = require('../query_parser');
var parser = Parser();
var Logger = require('../logger.js');
var Utils = require('./utils');
var ut = Utils();

module.exports = function(){	
	var _name = 'consultants';
	var _db = new DB();
	var _collection = _db.getCollection(_name);
	
	return {
		//--------------------------------------------------------------------------------------
		//	http post service, save a new consultant
		//--------------------------------------------------------------------------------------
		save: function(req, rsp){
			var c = req.body;
			var consultant = {username: c.username,
						password:c.password,
						email:c.email,
						program: c.program,
						school: c.school,
						photo: c.photo,
						role:2,
						description: c.description,
						service: c.service
					};
			
			 _collection.save(consultant, function(err, doc){
				 return rsp.json({ 'success': true, 'consultant': doc, error: null});
			 });
		},
		
		//--------------------------------------------------------------------------------------
		//	http post service
		//--------------------------------------------------------------------------------------
		
		get: function(req, rsp){
			//ut.checkToken(req, rsp, function(d){
				_collection.find(req.body, function(err, docs){
					for(var i in docs){
						delete docs[i].email;
					}
					return rsp.json({ success: true, consultants: docs});
				});
			//})
		},
		
		getDetail: function(req, rsp){
			//ut.checkToken(req, rsp, function(d){
				_collection.findOne(req.body, function(err, doc){
					delete doc.email;
					return rsp.json({ success: true, consultant: doc});
				});
			//})
		},
		
		//--------------------------------------------------------------------------------------
		// The $set operator replaces the value of a field with the specified value.
		// Arguments:
		// req --- http req object with body.query and body.updates fields. if the req is from
		//	mobile phone, it's a json string, if it's from web, it's a json object
		//	eg. { body: { query:{_id:xxx}, updates: { $set:{status: 0, created:ISODate() }}}}
		// callback --- function(err, doc)
		//--------------------------------------------------------------------------------------
		updateOne: function(req, rsp){
			var query = {};
			var updates = {};
			
			if(typeof req.body.query == 'string'){
				query = parser.parseQuery(JSON.parse(req.body.query));
			}else{
				query = parser.parseQuery(req.body.query);
			}
			
			if(typeof req.body.updates == 'string'){
				updates = JSON.parse(req.body.updates);
			}else{
				updates = req.body.updates;
			}
			
			if(Object.keys(updates['$set']).length != 0){
				// Arguments:
				// query 	--- { username, _id }
				// updates	--- The fields and values to be update, mongodb format: {$set:{k1:v1, k2:v2}
				_collection.update(query, updates, function(err, lastErrorObject){
					if(err){
						return rsp.json({ success: false, error: err});
					}else{
						return rsp.json({ success: true, error: err});
					}
				})
			}
		},
		
		//--------------------------------------------------------------------------------------
		// Arguments:
		// req --- http req object with fields. 
		//	if the req is from mobile phone, it's a json string. 
		//	if it's from web, it's a json object
		//	eg. { body: { query:{_id:xxx}, updates: { $set:{status: 0, created:ISODate() }}}}
		// callback --- function(err, doc)
		//--------------------------------------------------------------------------------------
		remove: function(req, rsp){
			var query = null;
			
			if(typeof req.body == 'string'){
				query = parser.parseQuery(JSON.parse(req.body));
			}else{
				query = parser.parseQuery(req.body);
			}
			if(query){
				_collection.remove(query, function(err, status){ // {n:, ok:}
					if(err){
						return rsp.json({ success: false, error: err});
					}else{
						return rsp.json({ success: true, error: err});
					}
				})
			}
		},
	}
}
