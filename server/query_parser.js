// The query_parser.js use to parse query object

var DB = require("./db");
var db = DB();

module.exports = function(){
	return {
		//--------------------------------------------------------------------------------------
		// parseQuery(query)
		//		parse all the fields to mongodb standard query object.
		// Arguments:
		// 		q --- [in] query
		//			type #1: {c1:x1, c2:x2 ... }
		//			type #2: {$and: [{c1:x1, c2:x2}, {c3:x3}], $or: [{c1:x1, c2:x2}, {c3:x3}]}
		//--------------------------------------------------------------------------------------
		parseQuery: function(q){
			var query = {};
			if(q!=null){
				
				for(key in q){
					var val = q[key];
					if(key.indexOf("_id", key.length - 3) > -1){ // parse the other fields end with "_id"
						if(val!=null && val !='' && typeof val == 'string'){
							query[key] = db.toObjectId(val);
						}else if('$in' in val){
							// convert ids to object_ids if needed
							var idArray = [];
							_.each(val['$in'], function(element, index, list){
								if(element !='' && typeof element == 'string'){
									idArray.push(db.toObjectId(element));
								}
							});
							query[key] = {'$in':idArray};
						}else{
							query[key] = val;
						}
					}else if(typeof val=='string' && (val=='null' || val=='') ){
						// Skip
					}else if('$include' in val){ 
						// parse {field:{'$include':val}} to {field:{'$include':RegExp(val, 'i')}
						query[key] = new RegExp(val['$include'],'i');
					}else{
						query[key] = val;
					}
				}
			}
			return query;
		}// end of parseQuery
	}
}