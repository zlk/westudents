Use mongoexport to export the data:

mongoexport --db westudents --collection traffic --out traffic.json


Use mongoimport to import the data:
mongoimport --db westudents --collection cities --file cities.json
mongoimport --db westudents --collection schools --file schools.json